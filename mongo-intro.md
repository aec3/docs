# Base de données 

![HTTP](./img/mongo1.webp)

## Introduction

Les bases de données sont des outils très utiles pour stocker et gérer des informations de manière organisée et structurée. 

Les bases de données permettent de stocker des informations de manière durable, d'y accéder facilement et rapidement, et de les modifier ou les supprimer selon les besoins. Elles permettent également d'effectuer des analyses complexes et des requêtes avancées sur les données, ce qui peut aider à prendre des décisions éclairées.

Le CRUD (Create, Read, Update, Delete) est un ensemble de fonctions de base utilisées dans la gestion de bases de données. Il s'agit de **créer** des données, de les **lire**, de les **mettre à jour** et de les **supprimer**. Ces opérations constituent les opérations fondamentales pour manipuler les données dans une base de données, et sont essentielles pour toutes les applications qui stockent des données.

## Pourquoi les bases de données sont importantes

La majorité des applications modernes utilisent des bases de données :

- Elles permettent de stocker des données de manière structurée et organisée, ce qui facilite l'accès et la manipulation des données.

- Elles permettent de stocker de grandes quantités de données et de les traiter rapidement, ce qui est essentiel pour les applications à grande échelle.

- Elles offrent une sécurité accrue en stockant les données dans des emplacements centralisés et en autorisant un accès contrôlé aux données.

## Serveur de base de données

Un serveur de base de données est un programme qui permet de stocker des données dans des fichiers sur le disque dur de l'ordinateur. Il permet également d'accéder aux données stockées dans les fichiers.
Le serveur de base de données tourne en permanence sur l'ordinateur et attend les requêtes des clients de base de données.

## Client de base de données

Un client de base de données est un programme qui permet d'exécuter des commandes pour interagir avec la base de données. Il permet d'envoyer des requêtes à la base de données et de recevoir des réponses.

Nos applications peuvent utiliser un client de base de données pour interagir avec la base de données. Les clients de base de données peuvent être des programmes écrits dans un langage de programmation, comme Python, Java ou JavaScript…

## Types de bases de données

### Base de données relationnelle

Les bases de données relationnelles : elles stockent les données dans des tables relationnelles avec des colonnes et des lignes. Les exemples de bases de données relationnelles incluent MySQL, PostgreSQL et Oracle.

SQL (Structured Query Language) est un langage de requête utilisé pour interagir avec les bases de données relationnelles. Il permet de créer, de lire, de mettre à jour et de supprimer des données dans les bases de données relationnelles.

### Base de données non relationnelle

Les bases de données non relationnelles : elles stockent les données dans des collections de documents. 

Ce sont des bases de données orientées "document".
**MongoDB** est un exemple populaire de système de gestion de base de données orientée document.

NoSQL (Not Only SQL) est un langage de requête utilisé pour interagir avec les bases de données orientées document. Il permet de créer, de lire, de mettre à jour et de supprimer des données dans les bases de données orientées document.

## MongoDB

Pour ce cours nous allons utiliser MongoDB.

MongoDb est un système de gestion de base de données orienté document.
- Créé en 2009.
- Il est open source.
- Il est multiplateforme (Windows, macOS, Linux).
- Il est gratuit.

https://www.mongodb.com/fr

Nous allons installer MongoDB sur notre ordinateur. Plusieurs programmes vont être installés :

- Installation du **serveur MongoDB** qui est un programme qui permettra de créer et de gérer des bases de données MongoDB sur votre machine. Une fois le serveur MongoDB installé, vous pourrez commencer à utiliser les commandes MongoDB pour interagir avec votre base de données et effectuer des opérations CRUD (Create, Read, Update, Delete) sur les collections de documents MongoDB.
- Installation de **MongoDB Shell (mongosh)** qui est un programme dans lequel vous pouvez exécuter des commandes pour interagir avec la base de données. Il permet d'envoyer des requêtes à la base de données et de recevoir des réponses.
- Installation de l'interface graphique **MongoDB Compass** qui est un programme qui permet de visualiser les données stockées dans la base de données.






