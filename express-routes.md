# Cours Express.js - Les routes

- Voir `demo1-routes` dans le dossier [demos-Express](https://livecegepfxgqc.sharepoint.com/:f:/s/AECDevWeb-2022-2024-2-Programmation1/EoRvpuqnahlEuMk1CPhf3lYBywYDIynnvMqZaumCoYDLsg?e=LGDDbH)

## Introduction

Dans Express.js, une route correspond à une action que l'utilisateur peut effectuer sur l'application web, comme l'affichage d'une page, la soumission d'un formulaire ou la récupération de données. Les routes sont définies en fonction du verbe HTTP utilisé (method) et de l'URL de la requête (path).

Les routes sont utilisées pour diriger les requêtes HTTP vers les fonctions appropriées dans votre application.
Chaque route peut avoir une ou plusieurs fonctions qui sont exécutées lorsque la route est mise en correspondance.

### Verbes HTTP

Les verbes HTTP sont des méthodes de requête qui définissent l'action à effectuer sur une ressource. Les verbes HTTP les plus courants sont :

- `GET` : récupère une ressource
- `POST` : crée une ressource
- `PUT` : met à jour une ressource
- `DELETE` : supprime une ressource

Cela correspond aux CRUD (Create, Read, Update, Delete) qui sont les opérations de base sur les données.


## Structure d'une route

La définition de route a la structure suivante :

```js
app.METHOD(PATH, HANDLER)
```

Où :
- `app` est une instance d'express.
- `METHOD` est une méthode de demande HTTP, en minuscules (get, post, put, delete)
- `PATH` est un chemin sur le serveur.
- `HANDLER` est la fonction exécutée lorsque la route est mise en correspondance.


## Définition d'une route

Pour définir une route dans Express.js, on utilise la méthode HTTP correspondante pour cette route. Par exemple, pour définir une route pour la méthode GET, on utilise la méthode `get` d'Express.js.

::: info
Les routes sont définies en utilisant les méthodes `app.get()`, `app.post()`, `app.put()` et `app.delete()`, selon le verbe HTTP utilisé.
:::

### Exemple de route pour une page d'accueil (`/`)

Route qui répond à une requête GET pour afficher une page d'accueil, par exemple avec l'url `https://monsite.com/` :

```js{4-6}
const express = require('express')
const app = express()

app.get('/', (req, res) => {
  res.send('Bienvenue sur mon site !')
})

app.listen(3000, () => {
  console.log('Le serveur écoute sur le port 3000')
})
```

Dans cet exemple, on utilise la méthode `get` pour définir une route qui correspond à la racine de notre site. Lorsqu'un utilisateur visite cette page, le serveur renverra la réponse "Bienvenue sur mon site !" en utilisant la méthode `send` de l'objet `res`.

### Exemple de route pour une page de profil (`/profil`)

Route qui répond à une requête GET pour afficher une page de profil qui serait à l'url `https://monsite.com/profil` :

```js
app.get('/profil', (req, res) => {
  res.send('Vous consultez votre profil')
})
```

### Exemple de route pour une page de soumission de  formulaire (`/formulaire`)

Route qui répond à une requête qui utilise la méthode POST pour traiter les données d'un formulaire qui sont envoyées à l'url `https://monsite.com/formulaire` :

```js
app.post('/formulaire', (req, res) => {
  res.send('Vous avez soumis le formulaire')
})
```

### Exemple de route pour une page de suppression de compte (`/supprimer`)

Route qui répond à une requête qui utilise la méthode DELETE pour supprimer un compte utilisateur. L'url pourrait être `https://monsite.com/supprimer` :

```js
app.delete('/supprimer', (req, res) => {
  res.send('Votre compte a été supprimé')
})
```
## Les paramètres de route

Les paramètres de route sont utilisés pour capturer des valeurs dynamiques dans l'URL d'une requête HTTP

Exemple : `https://monsite.com/users/2`

- `users` est un chemin de route
- `2` est un paramètre de route, il est "dynamique" et peut prendre n'importe quelle valeur

Avec cette url, l'utilisateur peut accéder, par exemple, à une page de profil pour l'utilisateur qui a l'identifiant 2.

Dans Express.js, vous pouvez définir des paramètres de route en utilisant deux points `:` avant le nom du paramètre.

Exemple : `/users/:id`

Dans cette URL, le paramètre de route est **:id**. Ce paramètre peut contenir n'importe quelle valeur dynamique spécifiée par l'utilisateur. 

Lorsque l'utilisateur soumet cette URL à votre serveur, Express.js analyse l'URL et stocke la valeur dynamique de `:id` dans l'objet `req.params.id`.


Voici un exemple de définition d'une route avec un paramètre qui est à l'url `https://monsite.com/users/2` :

```js
app.get('/users/:userId', (req, res) => {
  res.send(`Vous consultez le profil de l'utilisateur ${req.params.userId}`)
})
```

Dans cet exemple, nous avons défini une route avec un paramètre `userId`. Lorsqu'un utilisateur visite cette page avec un identifiant d'utilisateur spécifié, le serveur renverra la réponse "Vous consultez le profil de l'utilisateur 2".

Le `userId` est accessible via l'objet `req.params`.

## Les paramètres de requête

Les paramètres de requête sont des informations passées dans l'URL sous forme de clé-valeur. 

Les paramètres de requête sont inclus dans l'URL après le point d'interrogation `?`, et sont composés de paires clé-valeur séparées par des esperluette `&`. Par exemple, considérons l'URL suivante :

Exemple : `https://monsite.com/search?q=express&lang=fr`

Dans cette URL, les paramètres de requête sont `q=express` et `lang=fr`. La clé `q` a pour valeur `express`, et la clé `lang` a pour valeur `fr`. 

Dans Express.js, on récupére ces paramètres en utilisant l'objet `req.query`.

Voici un exemple de récupération de paramètres de requête :

```js
app.get('/search', (req, res) => {
  const searchTerm = req.query.q
  const lang = req.query.lang
  res.send(`Vous recherchez :  ${searchTerm} dans la langue ${lang}`)
})
```

Lorsqu'un utilisateur visite cette page avec un terme de recherche spécifié, le serveur renverra la réponse "Vous recherchez : [terme de recherche] dans la langue [langue]".
