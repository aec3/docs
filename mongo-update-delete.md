# Mise à jour et Suppression

## Mise à jour

### Mise à jour d'un document

Pour mettre à jour un document, on utilise la méthode `db.collection.updateOne()`

```javascript
db.etudiant.updateOne({nom: "Aminata"}, {$set: {age: 23}})
```

Met à jour le document dont le champ `nom` est égal à `Aminata` en changeant la valeur du champ `age` de `22` à `23`.

S'il y a plusieurs documents qui correspondent à la sélection, seul le premier sera mis à jour.


### Mise à jour multiple

Pour mettre à jour plusieurs documents, on utilise la méthode `db.collection.updateMany()`

```javascript
db.etudiant.updateMany({city: "Québec"}, {$set: {city: "Montréal"}})
```

Met à jour tous les documents dont le champ `city` est égal à `Québec` en changeant la valeur du champ `city` de `Québec` à `Montréal`.


::: tip Remarque
`updateOne()` mettra à jour le premier document fidèle à la sélection.

`updateMany()` modifiera tous les documents fidèles à la sélection.
:::

### unset

La méthode `unset` permet de supprimer un champ d'un document.

```javascript
db.etudiant.updateOne({nom: "Aminata"}, {$unset: {age: 1}})
```

Supprime le champ `age` du document dont le champ `nom` est égal à `Aminata`.

### inc

La méthode `inc` permet d'incrémenter ou de décrémenter une valeur numérique.

```javascript
db.etudiant.updateOne({nom: "Aminata"}, {$inc: {age: 1}})
```

Incrémente la valeur du champ `age` de `1` pour le document dont le champ `nom` est égal à `Aminata`.

## Suppression

### Suppression d'un document

Pour supprimer un document, on utilise la méthode `db.collection.deleteOne()`

```javascript
db.etudiant.deleteOne({nom: "Aminata"})
```

Supprime le document dont le champ `nom` est égal à `Aminata`.

S'il y a plusieurs documents qui correspondent à la sélection, seul le premier sera supprimé.

### Suppression multiple

Pour supprimer plusieurs documents, on utilise la méthode `db.collection.deleteMany()`

Supprime tous les documents dont le champ `city` est égal à `Québec`.
```javascript
db.etudiant.deleteMany({city: "Québec"})
```



Supprime tous les documents dont le champ age est supérieur ou égal à 19
```javascript
db.etudiant.deleteMany({age: {$gte: 19}})
```

## Exercice

::: tip Exercice

Avec la collection `products` utilisée dans l'exercice précédent, écrire les requêtes suivantes :

- 1. Mettre à jour le prix du produit avec le nom "Souris sans fil" à 39.99.
- 2. Mettre à jour le statut "en_stock" du produit avec le nom "Tapis de souris" à true.
- 3. Supprimer tous les produits qui ont une quantité de 0 en stock.
:::

## Solution

```javascript
db.products.updateOne({name: "Souris sans fil"}, {$set: {price: 39.99}})

db.products.updateOne({name: "Tapis de souris"}, {$set: {in_stock: true}})

db.products.deleteMany({quantity: 0})

```
