# Le Protocole HTTP

Le protocole HTTP (HyperText Transfer Protocol) est un protocole de communication utilisé pour l'échange de données sur le World Wide Web. Il permet aux clients (navigateurs web) de demander des ressources (pages web, images, fichiers, etc.) à des serveurs web, qui les fournissent en réponse.

## Modèle client-serveur

Le protocole HTTP est basé sur un modèle client-serveur. Les clients envoient des requêtes HTTP aux serveurs, qui renvoient des réponses HTTP en retour. Les requêtes et les réponses sont formatées sous forme de messages, qui contiennent des en-têtes (headers) et un corps (body).


![HTTP](./img/http1.png){data-zoomable}

## Les requêtes HTTP

Les requêtes HTTP sont des messages envoyés par le client au serveur. Elles sont composées de plusieurs parties :

- la ligne de requête, qui contient le nom de la méthode HTTP, l'URL de la ressource, et le protocole HTTP utilisé
- les en-têtes de requête, qui fournissent des informations supplémentaires sur la requête
- le corps de la requête, qui contient les données envoyées au serveur


## Les méthodes de requête HTTP

Il existe plusieurs méthodes de requête HTTP, chacune ayant une signification spécifique :

- GET : demande une ressource spécifique du serveur
- POST : envoie des données à un serveur pour traitement
- PUT : remplace une ressource existante sur le serveur
- DELETE : supprime une ressource existante sur le serveur

![HTTP](./img/http2.png){data-zoomable}
Exemples de requêtes HTTP quand on se connecte sur Omnivox.


## Les en-têtes HTTP

Les en-têtes HTTP sont des lignes de texte qui fournissent des informations supplémentaires sur une requête ou une réponse. Les en-têtes les plus courants sont :

- Host : indique le nom de domaine du serveur
- User-Agent : indique l'agent utilisateur du client
- Accept : indique le type de contenu que le client accepte
- Accept-Encoding : indique les encodages que le client accepte
- Accept-Language : indique les langues que le client accepte
- Cache-Control : indique si le client accepte ou non le cache
- Connection : indique si le client accepte ou non la connexion persistante
- Referer : indique l'URL de la page qui a envoyé la requête
- Authorization : contient les informations d'identification d'un client
- Content-Length : indique la taille du contenu de la requête
- Content-Type : indique le type de contenu de la requête
- Cookie : contient les cookies du client

![HTTP](./img/http4.png){data-zoomable}

## URL (Uniform Resource Locator)

![HTTP](./img/http3.png){data-zoomable}

Une URL est une chaîne de caractères qui identifie une ressource sur le web. Elle est composée de plusieurs parties :

- le protocole (http ou https)
- le sous-domaine (fr)
- le nom de domaine (wikipedia.org)
- le chemin d'accès à la ressource (path) (/wiki/Node.js)
- les paramètres (query string)
  - title=Node.js
  - action=edit


## Les réponses HTTP

Les réponses HTTP sont des messages envoyés par le serveur au client. Elles sont composées de plusieurs parties :

- la ligne de statut, qui contient le code de statut HTTP, et une courte description du code
- les en-têtes de réponse, qui fournissent des informations supplémentaires sur la réponse
- le corps de la réponse, qui contient les données envoyées par le serveur

![HTTP](./img/http5.png){data-zoomable}


## Les codes de statut HTTP

Le protocole HTTP est également basé sur l'utilisation de codes de statut, qui indiquent si une requête a été traitée avec succès ou non. Les codes de statut les plus courants sont :

- 200 OK : la requête a été traitée avec succès
- 404 Not Found : la ressource demandée n'a pas été trouvée sur le serveur
- 500 Internal Server Error : une erreur interne s'est produite sur le serveur
- 301 Moved Permanently : la ressource demandée a été déplacée de manière permanente vers une nouvelle URL
- 302 Found : la ressource demandée a été déplacée temporairement vers une nouvelle URL

[Liste des codes de statut HTTP](https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP)


## Requête HTTP et réponse HTTP

Voici un exemple de requête HTTP :

```http
GET /wiki/Node.js HTTP/2
Host: fr.wikipedia.org
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate, br
Referer: https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal
Connection: keep-alive
Cookie: WMF-Last-Access=10-Mar-2023; WMF-Last-Access-Global=10-Mar-2023; frwikiwmE-sessionTickLastTickTime=1678989347076; frwikiwmE-sessionTickTickCount=27; GeoIP=CA:QC:Qu__bec:46.95:-71.12:v4; frwikimwuser-sessionId=52564311bdad967de32c; WMF-DP=145
```

Voici un exemple de réponse HTTP :
```http
HTTP/2 200 OK
date: Thu, 10 Mar 2023 10:45:34 GMT
server: mw1413.eqiad.wmnet
x-content-type-options: nosniff
content-language: fr
vary: Accept-Encoding,Cookie,Authorization
last-modified: Thu, 10 Mar 2023 16:45:59 GMT
content-type: text/html; charset=UTF-8
content-encoding: gzip
age: 3008
x-cache: cp1079 hit, cp1085 miss
x-cache-status: hit-local
server-timing: cache;desc="hit-local", host;desc="cp1085"
strict-transport-security: max-age=106384710; includeSubDomains; preload
x-client-ip: 112.178.255.28
cache-control: private, s-maxage=0, max-age=0, must-revalidate
accept-ranges: bytes
X-Firefox-Spdy: h2
```

