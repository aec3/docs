# Configuration de mongoDb

## 1 - Créer un dossier pour stocker les données de mongoDb


- Naviguez vers le disque `C:` de votre ordinateur et créez un nouveau dossier `data`.
- Dans le dossier `data` créez un autre dossier `db`.

![HTTP](./img/mongo11.png)

Toutes les bases de données seront donc stockées dans le dossier `C:\data\db`.


## 2 - Ajouter le chemin du dossier d'installation de MongoDb dans les variables d'environnement

Les fichiers d'exécution de MongoDb sont stockés dans le dossier `bin` de l'installation de MongoDb. Pour pouvoir exécuter ces fichiers, nous devons ajouter le chemin du dossier `bin` dans les variables d'environnement.

Dans l'explorateur, naviguez vers le dossier d'installation de MongoDb et ouvrez le dossier `bin`.

Puis copier le chemin `C:\Program Files\MongoDB\Server\6.0\bin`

![HTTP](./img/mongo10.png){data-zoomable}


---

- Ouvrez le panneau de configuration de votre ordinateur.
- Cliquez sur `Système et sécurité` puis sur `Système`.

![HTTP](./img/mongo12.png)

![HTTP](./img/mongo13.png)

---
- Cliquez sur `Paramètres système avancés`.

![HTTP](./img/mongo14.png)

---
- Cliquez sur `Variables d'environnement`.

![HTTP](./img/mongo15.png)

---
- Dans la section `Variables système` sélectionnez la variable `Path` et cliquez sur `Modifier`.

![HTTP](./img/mongo16.png)

---
- Cliquez sur `Nouveau` et ajoutez le chemin du dossier `C:\Program Files\MongoDB\Server\6.0\bin` que vous avez créé précédemment.

![HTTP](./img/mongo17.png)

![HTTP](./img/mongo18.png)

---

## 3 - Exécuter le serveur de base de données

- Ouvrez un terminal et exécutez la commande `mongod` pour démarrer le serveur de base de données.

![HTTP](./img/mongo19.png)

---
Vous devriez voir le message suivant dans le terminal :

![HTTP](./img/mongo20.png){data-zoomable}

Cela signifie que le serveur de base de données est en cours d'exécution à l'adresse `127.0.0.1:27017` ou `localhost:27017`.


::: tip Bravo !!!
Vous venez d'installer un serveur de base de données MongoDB sur votre ordinateur.

Le serveur de base de données est maintenant en cours d'exécution. 

Il faut maintenant installer un **client** pour pouvoir interagir avec le serveur de base de données. 

Nous pouvons utiliser **Mongo Compass** qui est déjà installé sur votre ordinateur. C'est un interface graphique pour interagir avec le serveur de base de données.

Nous allons également utiliser **Mongosh** qui est une interface en ligne de commande mais nous devons l'intaller auparavant.
:::


<!-- 
::: tip À savoir
**MongoDb Compass** est une interface graphique pour interagir avec le serveur de base de données. Il est installé sur vtotre ordinateur.

**MongoDb Atlas** est un service cloud pour héberger des bases de données MongoDB. Il est gratuit pour les bases de données de moins de 512 Mo.

Nous utiliserons MongoDb Atlas plus tard dans le cours.
::: -->


