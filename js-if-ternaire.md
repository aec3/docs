# If ternaire

## Syntaxe

```js
condition ? valeurSiVrai : valeurSiFaux
```

## Exemple

```js
const age = 18;

const message = age >= 18 ? "Vous êtes majeur" : "Vous êtes mineur";

console.log(message); // "Vous êtes majeur"
```

Cette syntaxe est très utile dans les templates ejs par exemple :

```js
<% if (age >= 18) { %>
  <p>Vous êtes majeur</p>
<% } else { %>
  <p>Vous êtes mineur</p>
<% } %>
```

devient :

```js
<p><%= age >= 18 ? "Vous êtes majeur" : "Vous êtes mineur" %></p>
```

Exemple avec des class css

```js
<p class="<%= score < 0 ? "red" : "green" %>">Score : <%= score %></p>
```

Applique la class `red` si le score est inférieur à 0, sinon applique la class `green`.





