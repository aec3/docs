# Routes

Afin de rendre notre code plus lisible, nous allons déplacer les routes dans des fichiers séparés dans un dossier `routes`.

Voici par exemple l'arborecence d'un projet Express avec des fichiers de routes :
- admin.js pourrait contenir les routes pour la partie administration du site.
- posts.js pourrait contenir les routes pour afficher les articles de blogue (posts).

![HTTP](./img/router1.png)


## Création des fichiers de routes

Créez un fichier Javascript dans le dossier `routes`. Par exemple `index.js`.

Dans ce fichier, on importe `express` puis on utilise le router d'Express.
Ensuite on peut placer le code pour gérer les routes :

```js
const express = require('express');
// utilisation du routeur d'Express
const router = express.Router();

// Déclaration de la route pour la page d'accueil
// Utilise le template EJS `index.ejs`
router.get('/', (req, res, next) => {
    res.render('index', { title: 'Cegep' });
});

// exportation du routeur
exports.routes = router;
```

## Utilisation des fichiers de routes

Dans le fichier `app.js` :


```js
// importation du routeur
const indexRouter = require('./routes/index');

// Déclaration des routes qui commencent par `/`
app.use('/', indexRouter.routes);
```
---

## Démo 5

Voir `demo5-router-statics-files` dans le dossier [demos-Express](https://livecegepfxgqc.sharepoint.com/:f:/s/AECDevWeb-2022-2024-2-Programmation1/EoRvpuqnahlEuMk1CPhf3lYBywYDIynnvMqZaumCoYDLsg?e=LGDDbH).

Dans la démo 5, nous avons configuré les routes de l'application dans `app,js`.

![HTTP](./img/router2.png)

Dans les fichiers `admin.js` et `posts.js`, nous avons utilisé le routeur d'Express pour définir les routes de notre application.

![HTTP](./img/router3.png)

- 1- Déclaration du router d'Express.
- 2- Utilisation de l'objet `router` pour définir les routes.
- 3- Exportation du router pour pouvoir l'utiliser dans `app.js` avec `require`.


