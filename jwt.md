# Authentification

Dans un système REST, il n’y a pas de conservation de l’état de la session (stateless).
Chaque requête doit contenir tout ce qui est nécessaire pour être « comprise » par le serveur.

Deux solutions pour s’authentifier :
- Envoyer un nom utilisateur et un mot de passe avec chaque requête.
Le serveur doit vérifier l’authentification à chaque réception de requête.

- 👉 Utiliser un « token » fournit par le système d’authentification, et compréhensible par le serveur qui gère l’API. Utilisation du JWT (JSON Web Token)


![JWT](./img/jwt1.png){ data-zoomable }

## JWT (JSON Web Token)

L'authentification par JWT (JSON Web Token) est une méthode d'authentification qui permet de sécuriser les API REST.

L'authentification par JWT est basée sur le protocole OAuth 2.0.

## Principe

L'authentification par JWT est basée sur un échange de jetons entre le client et le serveur.

Le client envoie un jeton au serveur pour s'authentifier. Si le jeton est valide, le serveur renvoie un autre jeton au client.

Le jeton est un objet JSON qui contient des informations sur l'utilisateur.


## Structure du jeton

Un jeton JWT est un objet JSON qui contient trois parties séparées par un point `.`.

![JWT](./img/jwt2.png){ data-zoomable }

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQ1Njc4OTAiLCJuYW1lIjoiSm9obiBEb2UiLCJhZG1pbiI6dHJ1ZX0.B4gevSzluIGuz4Tixn3kfdEy2ExMpvCQBlYLrjg3V00
```

Voir le site https://jwt.io/ qui permet de décoder un jeton.

### Header (rouge)

Le header contient le type du jeton et l'algorithme de signature utilisé.

```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```

### Payload (mauve)

Le payload contient les informations sur l'utilisateur.

```json
{
  "id": "1234567890",
  "name": "John Doe",
  "admin": true
}
```


### Signature (bleu)

La signature est une chaîne de caractères encodée en base64 qui permet de vérifier l'intégrité du jeton.

Le mot de passe pour ce jeton est `password`.

## Exemple d'implémentation

Pour utiliser les tokens JWT avec Express.js, on utilise le module `jsonwebtoken`.

```js
npm install --save jsonwebtoken
```

### Création d'un token

La méthode `sign()` permet de créer un token. Elle prend en paramètre un objet JSON qui contient les informations à stocker dans le token, une clé secrète et un objet JSON qui contient des options.

La clé secrète est dans une variable d'environnement (SECRET_JWT) dans le fichier `.env`.

```js
const jwt = require('jsonwebtoken');

//...

// Création du token
const token = jwt.sign(
  {
    // Payload, on met ici les informations que l'on veut stocker dans le token
    userId: user._id,
    email: email
  },
  process.env.SECRET_JWT,
  { expiresIn: '1h' }
);

```

### Vérification d'un token

La méthode `verify()` permet de vérifier un token. Elle prend en paramètre un token et la clé secrète.

```js
// Vérification d'un token
decodedToken = jwt.verify(token, process.env.SECRET_JWT);
```

## Middleware d'authentification

Pour les routes que l’on souhaite protéger avec l’authentification, on peut ajouter un middleware qui vérifie le JWT.

```js
// importe le middleware
const isAuth = require('../middleware/is-auth’);

// utilise le middleware avant d’executer le controlleur
router.post('/post/', isAuth, postsController.createPost);

```

Voir la démo `demo11-jwt` pour analyser le middleware `is-auth`

## Tester l'API avec Postman

Pour tester l'API avec Postman, il faut ajouter le token dans le header de la requête.

Dans l'onglet `Auth`, choisir `Bearer Token` et coller le token dans le champ Token, ou utiliser une variable.

![JWT](./img/jwt3.png){ data-zoomable }

