# Fichier statique

Les fichiers statiques sont des fichiers qui ne sont pas générés par le serveur. Par exemple, les fichiers HTML, CSS, JavaScript, images, etc. sont des fichiers statiques.

Pour servir des fichiers statiques avec Express.js, il faut utiliser le middleware `express.static()`.

Par convention, les fichiers statiques sont placés dans un dossier `public` à la racine de l'application.

```js
app.use(express.static(path.join(__dirname, 'public')));
```

- `path.join()` permet de créer un chemin de fichier en concaténant les arguments passés en paramètre.

- `__dirname` est une variable globale qui contient le chemin du dossier courant.

Dans cet exemple, le dossier `public` est placé à la racine de l'application.

Dans le dossier `public`, on crée des dossiers pour les fichiers statiques, par exemple :

- `css` pour les fichiers CSS
- `js` pour les fichiers JavaScript
- `images` pour les fichiers images

Dans l'exemple ci-dessous, on peut voir que le fichier de style `main.css` utilisé dans le template `head.ejs`est dans le dossier `css`.

Express est capable de le trouver dans le dossier `public` car on a utilisé le middleware `express.static()`.

![HTTP](./img/static1.png)

Voir `demo5-router-statics-files` dans le dossier [demos-Express](https://livecegepfxgqc.sharepoint.com/:f:/s/AECDevWeb-2022-2024-2-Programmation1/EoRvpuqnahlEuMk1CPhf3lYBywYDIynnvMqZaumCoYDLsg?e=LGDDbH).