---
# https://vitepress.vuejs.org/reference/default-theme-home-page
layout: home

hero:
  name: "AEC développement web"
  text: "Programmation 1"
  # tagline: My great project tagline
  actions:
    - theme: brand
      text: Cours en ligne
      link: /dom
    # - theme: alt
    #   text: API Examples
    #   link: /api-examples

features:
  - title: Cours sur Teams 
    icon: 
      src: "https://upload.wikimedia.org/wikipedia/commons/c/c9/Microsoft_Office_Teams_%282018%E2%80%93present%29.svg"
    details: Accés aux cours format Pdf et PowerPoint.
    link: https://livecegepfxgqc.sharepoint.com/:f:/s/AECDevWeb-2022-2024-2-Programmation1/EsphxkCPJS1ItheICbIRtaUBu6J21eeZQ3glb5PfzbEZKA?e=Rb9Ddt
  - title: Lien vers l'équipe Teams
    icon: 
      src: "https://upload.wikimedia.org/wikipedia/commons/c/c9/Microsoft_Office_Teams_%282018%E2%80%93present%29.svg"
    # details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
    link: https://teams.microsoft.com/l/team/19%3abcEFX2lN3kGmXf0mwZtlfEF9u3qTUjkMNSyUx8lnJS41%40thread.tacv2/conversations?groupId=141ee9fa-4417-4bcb-a522-2f3b3361ef50&tenantId=c4a847fa-1e2c-4d94-a1ac-2ed81ddb600a
#   - title: Feature C
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---

