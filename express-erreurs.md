# Gestion des erreurs

Documentation : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Statements/throw

L'instruction `throw` permet de *lever une exception*.

*Lever une exception* permet de gérer les erreurs et de les traiter afin de ne pas interrompre l'exécution du programme et de renvoyer un message d'erreur pour informer l'utilisateur.

L'instruction `throw` permet donc de gérer les erreurs et de les traiter.

## Gestion des erreurs dans Express.js

- Express est livré avec un gestionnaire d'erreurs intégré qui prend en charge toutes les erreurs qui pourraient être rencontrées dans l'application. 
- Cette fonction middleware de gestion des erreurs par défaut est ajoutée à la fin de la pile de fonctions middleware.
- Si une erreur est passée à next() et qu’elle n’est pas traitée par un gestionnaire d'erreurs personnalisé, elle sera traitée par le gestionnaire d'erreurs intégré.
l'erreur sera écrite au client avec la trace de la pile.
- La trace de la pile n'est pas incluse dans l'environnement de production.	

## Erreurs dans du code synchrone

Pour les erreurs qui se produisent dans le code synchrone, ou si on souhaite « lever » une erreur, Express les « attrape » et les traite avec `throw`

Exemple: si un utilisateur n'a pas de token valide, la variable `decodedToken` est à `false`.

On crée un objet `Error` avec un message.

On peut ajouter un code de status, par exemple `401` (Unauthorized).

Puis on lève une exception avec `throw` et on lui passe l'objet `error`.

```js
if (!decodedToken) {
  const error = new Error('Non authentifié.');
  error.statusCode = 401;
  throw error;
}
```

## Erreurs dans du code asynchrone


Pour les erreurs qui se produisent dans le code asynchrone, c'est à dire quand vous avez un `.catch`, on les « attrape » avec `next()` et on les traite dans le middleware d'erreur.

```js
Post.findById(postId)
.then(post => {
  // code...
})
.catch(err => {
  next(err);
});
```

## Middleware d'erreur

Le middleware d'erreur est un middleware qui prend 4 paramètres. Il est exécuté si une erreur est levée dans le code asynchrone.

```js
app.use((err, req, res, next) => {
  res
    .status(err.status || 500)
    .json({
      error: {
        message: err.message
      }
    });
});
```


## Exemple d'utilisation de `throw` avec `try` et `catch`

```js
function division(a, b) {
  if (b === 0) {
    throw new Error('Division par zéro');
  }
  return a / b;
}
```

```js
try {
  const result = division(10, 0);
  console.log(result);
} catch (error) {
  console.log(error.message);
}
```


## Sources

https://expressjs.com/en/guide/error-handling.html
