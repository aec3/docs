# API avec Express

Dans ce chapitre, nous allons voir comment créer une API avec Express.

## Supprimer les vues

Une API n'a pas besoin de vues donc nous allons donc supprimer le dossier `views` et tout le code qui concerne les vues.

Nous supprimons également les fichiers CSS.

## Middleware JSON

Notre API va utiliser le format JSON pour les données. Il est donc important de pouvoir envoyer et recevoir des données au format JSON.

Pour cela il faut utiliser le middleware `express.json()`. Ce middleware va transformer les données JSON en objet JavaScript et inversement.

Ce middleware permet de récupérer les données envoyées par le client dans `req.body`.

Dans le fichier `app.js` ajouter le middleware `express.json()`.

```js
app.use(express.json()); 
```

## Routes

Voici les routes que nous allons créer pour notre API.

| Méthode | Route | Description |
| --- | --- | --- |
| GET | /articles | Récupérer tous les articles |
| GET | /articles/:articleId | Récupérer un article |
| POST | /articles | Créer un article |
| PUT | /articles/:articleId | Modifier un article |
| DELETE | /articles/:articleId | Supprimer un article |

---

![Routes](./img/sw11.png){ data-zoomable }

On remarque qu'on utilise maintenant tous les verbes HTTP `GET`, `POST`, `PUT` et `DELETE`. (soulignés en rouge)

Les routes (endpoints) sont souligées en bleu.

## Contrôleur

La partie contrôleur doit maintenant gérer les requêtes et les réponses au format JSON.

Nous remplaçons donc le code du contrôleur qui renvoyait une vue par du code qui renvoie des données JSON.

Voici le code que nous avions dans le contrôleur `articles.js` :

![Contrôleur](./img/sw12.png)

Nous le remplaçons par le code suivant :

![Contrôleur](./img/sw13.png)

La méthode `json()` permet d'envoyer des données au format JSON.

Le code `200` est envoyé par défaut par Express. Il n'est donc pas nécessaire de le préciser.

Celui-ci signifie que la requête s'est bien déroulée.

Pour une création, on utilise le code `201` qui signifie que la ressource a bien été créée.

Pour une suppression, on utilise le code `204` qui signifie que la ressource a bien été supprimée.

Voir la page de [code de réponse HTTP](https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP) sur Wikipédia.


Voir le reste du code dans la demo10.

## Tester l'API

Il faut utiliser Postman pour tester l'API.

![Postman](./img/sw14.png){ data-zoomable }

- 1. Créer une collection afin de regrouper les requêtes.
- 2. Séléctionner l'onglet `Variables`.
- 3. Créer une variable `base_url` qui contient l'URL de base de l'API. Cette variable sera utilisée dans toutes les requêtes et permettra de changer facilement l'URL de l'API.
- 4. Créer un dossier pour chaque type de requête. (Ici pour les articles par exemple)

Exemple pour la requête `GET /article` :

![Postman](./img/sw15.png){ data-zoomable }

Pour utiliser la variable, il faut utiliser la syntaxe suivante `{{base_url}}`.

## Exercice

Créer une API pour gérer des catégories.

Le modèle de données est le suivant :

| Propriété | Type | Description |
| --- | --- | --- |
| name | String | Nom de la catégorie (obligatoire) |

Voici les routes à créer :

| Méthode | Route | Description |
| --- | --- | --- |
| GET | /categories | Récupérer toutes les catégories |
| GET | /categories/:categoryId | Récupérer une catégorie |
| POST | /categories | Créer une catégorie |
| PUT | /categories/:categoryId | Modifier une catégorie |
| DELETE | /categories/:categoryId | Supprimer une catégorie |
