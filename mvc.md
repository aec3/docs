# MVC Modèle - Vue - Contrôleur

## Introduction 



L'architecture MVC (Modèle-Vue-Contrôleur) est un modèle d'architecture logicielle qui divise une application en trois composants principaux : le modèle, la vue et le contrôleur. Cette architecture est couramment utilisée pour le développement de logiciels web.

Le **modèle** représente les données et la logique de l'application. Cela inclut la gestion de la base de données, la récupération des données, la validation des données, etc.

La **vue** est la représentation graphique de l'application. Cela inclut tout ce que l'utilisateur peut voir et avec quoi il peut interagir. Les vues peuvent être des pages HTML, des formulaires, des graphiques, etc.

Le **contrôleur** est le composant qui reçoit les demandes de l'utilisateur et les traite en utilisant les modèles et les vues appropriés. Cela inclut la gestion des entrées utilisateur, la récupération des données du modèle et la sélection de la vue appropriée pour afficher les résultats.

L'architecture MVC permet une séparation claire des responsabilités, ce qui facilite la maintenance et l'évolutivité du code. De plus, elle permet également de mieux organiser le code, ce qui peut rendre le développement plus rapide et plus efficace.

![mvc](./img/mvc1.png)
source: https://fr.wikipedia.org/wiki/Mod%C3%A8le-vue-contr%C3%B4leur

## Structure de l'application Express.js 

Pour suivre le modèle MVC, nous allons modifier la structure de notre application Express.js. 

- Nous allons créer un dossier `controllers` qui contiendra tous les contrôleurs de notre application.

- Nous allons également créer un dossier `models` qui contiendra tous les modèles de notre application. 

- Enfin, nous avons déjà créé un dossier `views` qui contient toutes les vues de notre application.


Voici à quoi va ressembler la structure de notre application Express.js :

![mvc](./img/mvc2.png){data-zoomable}

---

![mvc](./img/mvc3.png){data-zoomable}
