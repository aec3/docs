# Postman

https://www.postman.com/

Postman est un outil qui permet de tester des services Web.

Il permet de faire des requêtes HTTP et de voir les réponses.

Il permet aussi de créer des collections de requêtes pour tester un service Web.

Il existe des alternatives à Postman, comme par exemple [Insomnia](https://insomnia.rest/) ou [Hoppscotch](https://hoppscotch.io/).

## Installation

Postman est disponible sur Windows, Mac et Linux.

Il est possible de l'installer sur votre ordinateur ou de l'utiliser en ligne.

Pour le cours, il est préférable de l'intaller sur votre ordinateur.

Pour l'installer, rendez-vous sur le site officiel : https://www.postman.com/downloads/

## Utilisation

Pour tester Postman, nous allons utiliser une API de test : https://reqres.in/ qui renvoie des données aléatoires.

### Créer une collection

Afin de regrouper les requêtes, il est possible de créer des collections.

Pour créer une collection, il faut cliquer sur le bouton `New`.

![Postman](./img/sw5.png){data-zoomable width=75% }

Puis choisir `Collection`.

![Postman](./img/sw7.png){data-zoomable width=75% }

La nouvelle collection apparaît dans la liste des collections.

Vous pouvez la renommer faisant un clic droit sur la collection.

![Postman](./img/sw8.png){data-zoomable width=75% }


### Créer une requête

En faisant un clic droit sur la collection, il est possible de créer une requête.

![Postman](./img/sw6.png){data-zoomable width=75% }

Pour créer une requête, il faut :

- Choisir la méthode HTTP
- Saisir une l'URL

Exemple :
`GET https://reqres.in/api/users`

![Postman](./img/sw9.png){data-zoomable width=75% }

- 1 : Méthode HTTP
- 2 : URL
- 3 : Nom de la requête que vous choisissez
- 4 : Save pour sauvegader la requête

### Envoyer une requête

Pour envoyer une requête, il faut cliquer sur le bouton `Send`.

La réponse s'affiche dans la partie inférieure de la fenêtre.

![Postman](./img/sw10.png){data-zoomable width=75% }

- 1 : Bouton `Send`
- 2 : Réponse de la requête

Ici on voit que la requête a renvoyé un code 200, ce qui signifie que la requête a réussi.
On voit également la réponse de la requête, c'est à dire une liste d'utilisateur.


## À faire

Rechercher un service Web et l'essayer avec Postman.

Voici une liste d'API publiques : 
- https://github.com/public-apis/public-apis



