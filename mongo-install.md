# Installation de mongoDb

## 1 - Télécharger le package d'installation MSI MongoDB

https://www.mongodb.com/try/download/community

Assurez-vous de sélectionner MSI comme package à télécharger.

![HTTP](./img/mongo2.png){data-zoomable}

## 2 - Installer MongoDB

Lancez le package d'installation téléchargé.

Suivre les étapes d'installation.

![HTTP](./img/mongo3.png){data-zoomable}

---
![HTTP](./img/mongo4.png)

---
![HTTP](./img/mongo5.png)

---
![HTTP](./img/mongo6.png)

---
![HTTP](./img/mongo7.png)

---
![HTTP](./img/mongo8.png)

---
![HTTP](./img/mongo9.png)

::: tip Terminée !
L'installation est terminée. Vous pouvez maintenant configurer MongoDB.
:::




