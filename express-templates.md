# Templates

Les templates sont des fichiers HTML qui contiennent des balises qui permettent d'insérer du code JavaScript dans le HTML.

Il existe plusieurs moteurs de templates, les plus populaires sont :

![HTTP](./img/templates1.png){data-zoomable}

Chaque moteur de template a ses propres balises. 

Dans ce cours nous utiliserons le moteur de template EJS.

## Templates EJS

[Documentation](https://ejs.co/)

[Documentation avec Express](https://github.com/mde/ejs/wiki/Using-EJS-with-Express)

Les templates EJS sont des fichiers HTML qui contiennent des balises EJS. Ces balises permettent d'insérer du code JavaScript dans le HTML.

Les balises EJS sont entourées par des chevrons `<%` et `%>`.

Les balises EJS peuvent être de deux types :
- `<%=` : insère le résultat d'une expression JavaScript
- `<%` : insère du code JavaScript

Exemple d'utilisation d'une balise EJS :

```html
<h1><%= title %></h1>
```

La balise EJS `<%= title %>` va insérer la valeur de la variable `title` dans le HTML.

Exemple d'utilisation d'une balise EJS :

```html
<% if (title === 'Cegep') { %>
    <h1><%= title %></h1>
<% } %>

```

La balise EJS `<% if (title === 'Cegep') { %>` va insérer du code JavaScript dans le HTML.

## Utilisation des templates EJS

Pour utiliser les templates EJS, il faut installer le module `ejs` :

```bash
npm install --save ejs
```

Pour utiliser les templates EJS, il faut configurer le moteur de template EJS dans le fichier `app.js` avec la commande suivante :

```js
// Configuration du moteur de template EJS
app.set('view engine', 'ejs');
```

::: tip Important
Les fichiers de templates EJS doivent être placés dans un dossier `views` à la racine de l'application.
:::

Pour utiliser un template EJS, il faut utiliser la méthode `res.render()` et spécifier le nom du template EJS et les variables qui seront utilisées dans le template EJS :

```js
res.render('index', { title: 'Cegep' });
```
Dans cet exemple, le template EJS `index.ejs` sera utilisé et la variable `title` aura la valeur `Cegep`.

La méthode `res.render()` prend deux paramètres :
- le nom du template EJS
- un objet qui contient les variables qui seront utilisées dans le template EJS

### Exemple d'utilisation des templates EJS avec Express.js

```js
const express = require('express');
const app = express();

// Configuration du moteur de template EJS
app.set('view engine', 'ejs');

app.get('/', (req, res, next) => {
  res.render('index', { title: 'Cegep' });
});

app.listen(3000);
```

### Exemple de template EJS
Exemple de fichier `index.ejs` dans le dossier `views` :

```html

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><%= title %></title>
</head>
<body>
    <h1><%= title %></h1>
</body>
</html>
```
Voici l'arborescence du projet :

![HTTP](./img/templates2.png)

---
### Exemple de template EJS avec une boucle

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><%= title %></title>
</head>

<body>
    <h1><%= title %></h1>
    <ul>
        <% for (let i = 0; i < 10; i++) { %>
            <li><%= i %></li>
        <% } %>
    </ul>
</body>
```

Voir `demo4-template-html-ejs` dans le dossier [demos-Express](https://livecegepfxgqc.sharepoint.com/:f:/s/AECDevWeb-2022-2024-2-Programmation1/EoRvpuqnahlEuMk1CPhf3lYBywYDIynnvMqZaumCoYDLsg?e=LGDDbH) pour un exemple complet.




## Utilisation des "Includes"

Les "includes"  permettent d'inclure du contenu réutilisable dans un template. Pour inclure un fichier dans un template EJS, il faut utiliser la balise `<%- include('nom-du-fichier') %>`.

Le nom du fichier peut être soit un chemin absolu, soit un chemin relatif à partir du fichier courant.

Par exemple, supposons que nous avons trois fichiers contenant le code HTML pour :
- l'en-tête (`head.ejs`),
- la navigation (`navigation.ejs`),
- la fin d'une page (`end.ejs`).

On peut inclure ces fichiers dans un template principal en utilisant les balises include `<%-`.

Exemple d'arborescence avec un dossier `includes` qui contient des fichiers EJS :

![HTTP](./img/templates3.png)

---

Exemple de contenu des trois fichiers EJS :

![HTTP](./img/templates4.png)

---

Exemple d'utilisation des fichiers EJS dans le fichier `index.ejs` :

```html
<%- include('includes/head.ejs') %>

    <%- include('includes/navigation.ejs') %>
    <h1>Page d'acceuil</h1>

<%- include('includes/end.ejs') %>
```

