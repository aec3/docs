# Postman scripts et variables

## Script de pré-requête et script de test

[Documentation](https://learning.postman.com/docs/writing-scripts/pre-request-scripts/)

Postman permet d'exécuter des scripts avant et après l'exécution d'une requête.

Le Script de pré-requête (pre-request script) est exécuté avant l'exécution de la requête.

Le script de test est exécuté après l'exécution de la requête. Il peut être utilisé faire des tests afin de valider la réponse.

Les scripts sont écrits en JavaScript.

Nous verrons prochainement comment utiliser les scritps de test pour valider la réponse de l'API.

![Scripts](./img/sw17.png){ data-zoomable}

## Variables

[Documentation](https://learning.postman.com/docs/sending-requests/variables/)

Postman permet de définir des variables pour les requêtes. Ces variables peuvent être utilisées dans les requêtes, les tests et les scripts.

Il existe plusieurs types de variables :

- Variables d'environnement
- Variables de collection
- Variables globales
- Variables locales

Nous utiliserons les **variables de collection** qui sont les plus adaptées à notre cas d'utilisation.

## Définir une variable

Les variables peuvent être définies dans l'onglet `Variables` de Postman quand vous êtes dans une collection.

![Variables](./img/sw16.png){ data-zoomable}


## Utiliser une variable

### Dans une requête

Les variables peuvent être utilisées dans les requêtes en utilisant la syntaxe `{{variable}}`.

### Dans un scripts de pré-requête ou un test

Dans les scripts de pré-requête ou les tests, nous pourrons les utiliser en utilisant la syntaxe `pm.collectionVariables.get("variable")`.


## Génération de données aléatoires

Postman utilise la bibliothèque [faker](https://www.npmjs.com/package/@faker-js/faker) pour générer des échantillons de données, notamment des noms, des adresses, des adresses électroniques et bien d'autres choses encore. Vous pouvez utiliser ces variables prédéfinies plusieurs fois pour renvoyer des valeurs différentes par requête.

Vous pouvez utiliser ces variables comme n'importe quelle autre variable dans Postman. Leurs valeurs sont générées au moment de l'exécution et leurs noms commencent par le symbole `$`, par exemple `$guid` ou `$timestamp`.

Voici une liste de variables dynamiques dont les valeurs sont générées de manière aléatoire au cours de l'exécution de la demande ou de la collecte.

[Liste des variables](https://learning.postman.com/docs/writing-scripts/script-references/variables-list/)

Pour utiliser des variables dynamiques dans des scripts de pré-requête ou de test, vous devez utiliser `pm.variables.replaceIn()`.

Exemple:
```js
const nomAleatoire = pm.variables.replaceIn('{{$randomFirstName}}')
```

## Exemple

Nous voulons générer le titre d'un article et une description aléatoire pour chaque requête.

On choisit la variable `randomProductName` pour le titre, cela donne le nom d'un produit aléatoire mais c'est suffisant pour notre exemple.

### Script de pré-requête

![Variables](./img/sw18.png){ data-zoomable}

Dans le script de pré-requête
- ligne1 : nous définissons une constante `randomProductName` qui contient un nom généré aléatoirement par `$randomProductName`.
- ligne 3 : nous définissons une variable de collection `randomProductName` dans laquelle nous stockons la valeur de la constante `randomProductName`.

La variable de collection pourra être utilisée dans d'autre script comme le body de la requête par exemple.

### body de la requête

- 1. Pour le titre de l'article et l'url de l'image, nous utiliserons la variable de collection `randomProductName` que nous avons définie dans le script de pré-requête.

- 2. Pour la description, nous utiliserons la variable dynamique `$randomLoremParagraph` qui permet de générer un paragraphe de texte aléatoire.

![Variables](./img/sw19.png){ data-zoomable}

---

**Important**

Remarquez la différence entre les deux variables :
- `$randomLoremParagraph` est une variable dynamique qui sera générée à chaque requête. Il y a le symbole `$` au début du nom de la variable.
Cette variable ne sera pas utilisable dans d'autres scripts, elle n'est pas stockée.

- `randomProductName` est une variable de collection qui sera générée une seule fois dans le script de pré-requête. Elle est stockée dans la collection. 
Nous pouvons ainsi garder une trace de la valeur de la variable et l'utiliser dans d'autres scripts, en particulier dans les tests.

