# MongoDB Atlas

MongoDB Atlas est une plateforme de base de données cloud gérée par MongoDB, la société à l'origine de la base de données MongoDB.

Atlas offre une solution pour héberger les bases de données MongoDB dans le cloud.

## Créer un compte

https://www.mongodb.com/atlas/database

- Créer un compte gratuit et connectez-vous.
- Sélectionnez l’option Shared qui est gratuite.

![MongoDB Atlas](./img/mongo37.png)

## Créer un cluster

Créer un cluster, vous pouvez laisser les options par défaut.

![MongoDB Atlas](./img/mongo38.png)

## Identifiant

Choisissez un *Username* et *Password* qui permettront d’accéder à la base de données.

![MongoDB Atlas](./img/mongo39.png)

## Adresse ip de l’API

- Saisir une adresse ip à partir de laquelle on veut accéder à la base de données.
- Pour le moment on ne connaît pas l’adresse ip de notre API.
- On saisi 0.0.0.0 pour autoriser l’accès à partir de n’importe quel serveur.
- Pour une « vraie » application, il faudrait mettre une vraie adresse ip pour des raisons de sécurité

![MongoDB Atlas](./img/mongo40.png)

## Information de connexion

Une fois que la base de données est prête, on peut aller dans le menu *Databases*, puis cliquez sur *Connect* afin de voir les informations de connexion.

![MongoDB Atlas](./img/mongo41.png)

---

![MongoDB Atlas](./img/mongo42.png)

---

![MongoDB Atlas](./img/mongo43.png)

---

Copier l’adresse, nous allons l’utiliser dans la configuration de notre projet.

Il faudra penser à modifier **password** par celui choisi lors de la création de la base de données

::: danger Attention !
Il faut ajouter le nom de la collection à la fin de l’adresse, par exemple **TP3_produits**.
:::

![MongoDB Atlas](./img/mongo44.png){ data-zoomable }


