# Mongoose

[Documentation](https://mongoosejs.com/docs/guide.html)

Mongoose est une bibliothèque JavaScript qui facilite l'interaction avec les bases de données MongoDB à partir d'applications Node.js.

Mongoose simplifie l'interaction entre l'application et la base de données mongoDb en fournissant des fonctionnalités utiles telles que la définition des données, la validation et la gestion des erreurs.

Mongoose est un mappeur objet-document (ODM), cela permet de manipuler les données de la base de données MongoDB comme des objets JavaScript.

L'ODM se charge ensuite de traduire ces objets en documents JSON (ou BSON, le format binaire équivalent) qui sont stockés dans la base de données mongoDb.

## Installation

Pour installer Mongoose, exécutez la commande suivante dans le terminal :

```bash
npm install --save mongoose
```

## Connexion à la base de données

Pour se connecter à une base de données MongoDB, on utilise la méthode `connect()` de Mongoose.

```js
const mongoose = require('mongoose')
//...
mongoose.connect('mongodb://localhost:27017/ma-base-de-donnees')
```

- `mongodb://localhost:27017/` est l'URL du serveur MongoDB installé sur votre ordinateur.
- `ma-base-de-donnees` est le nom de la base de données que vous souhaitez utiliser.

Pour être sûr que la connexion à la base de données est établie avant de démarrer le serveur Express, on peut utiliser les promesses :

```js
const mongoose = require('mongoose')
//...
mongoose.connect('mongodb://localhost:27017/ma-base-de-donnees')
  .then(() => {
    console.log('La connexion à la base de données est établie')

    app.listen(3000, () => {
      console.log('Le serveur écoute sur le port 3000');
    });
  })
  .catch(err => {
    console.log('La connexion à la base de données a échoué', err)
  })
```

::: danger Attention
Assurez-vous que le serveur MongoDB est en cours d'exécution sur votre ordinateur avant de lancer votre application Node.js.

Vous devez exécuter la commande `mongod` dans un terminal.
:::

## Définir un modèle

Un modèle Mongoose est un objet qui définit la structure des documents qui seront stockés dans la base de données.

Cela s'appelle un schéma.

Il faut créer un modèle pour chaque collection de la base de données.

Par exemple, si nous avons un collection `users` et une autre collection `posts` dans notre base de données, il faudra créer un modèle pour chaque collection.

Par convention, on crée un dossier `models` qui contiendra tous les modèles de notre application.

Dans la capture ci-dessous on voit que deux fichiers `user.js` et `post.js` ont été créés dans le dossier `models`.

![collections](./img/mongoose1.png)



Pour créer un schéma, on utilise la fonction `Schema()` de Mongoose.

Exemple pour le fichier `user.js` :

```js:line-numbers {1}
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: String,
  email: String,
  age: Number
})

module.exports = mongoose.model('User', userSchema)
```

- Ligne 4-8: on définit le schéma du modèle `User`.
- Ligne 10: on exporte le modèle `User` en utilisant la méthode `model()` de Mongoose.




## Définir un modèle avec des validations

On peut ajouter des validations aux champs du schéma.

Par exemple, on peut ajouter une validation pour le champ `name` afin de s'assurer que le nom de l'utilisateur ne soit pas vide.

Pour le champs `email`, on peut ajouter une validation pour s'assurer que l'adresse email ne soit pas vide et soit unique.

Avant d'écrire dans la base de données, Mongoose vérifie que les données sont valides.


```js:line-numbers {1}
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true,
      unique: true
    },
    age: Number
  },
  { timestamps: true }
);

module.exports = mongoose.model('User', userSchema)
```

- Ligne 17: permet de créer automatiquement les champs `createdAt` et `updatedAt` dans la base de données. 

--- 

## Créer un document

Par exemple suite à la création d'un formulaire d'inscription, on va pouvoir récupérer les données saisies par l'utilisateur puis les enregistrer dans la base de données.

Pour créer un document, on va pouvoir importer le modèle `User`.

`User` va pouvoir être utilisé comme une classe et on va pouvoir créer des instances de `User` avec le mot-clé `new`.

Puis on va pouvoir utiliser la méthode `save()` pour enregistrer le document dans la base de données.

```js:line-numbers {1}
const User = require('./models/user')
//...
const user = new User({
  name: 'John Doe',
  email: 'test@test.ca',
  age: 30
})

user.save()
  .then(result => {
    console.log(result)
  })
  .catch(err => {
    console.log(err)
  })
```

- Ligne 1: on importe le modèle `User`.
- Ligne 3-7: on crée une instance de `User` avec le mot-clé `new`.
- Ligne 9-14: on enregistre le document dans la base de données avec la méthode `save()`.

Si les données ne sont pas valides, Mongoose renvoie une erreur de validation dans la variable `err`.

## Récupérer tous les documents

Pour lire des documents, on utilise la méthode `find()`.

`find()` renvoie un tableau contenant tous les documents de la collection.

```js:line-numbers {1}
const User = require('./models/user')
//...
User.find()
  .then(result => {
    console.log(result)
  })
  .catch(err => {
    console.log(err)
  })
```

## Récupérer un seul document

Pour lire un document, on utilise la méthode `findOne()`.

`findOne()` renvoie le premier document qui correspond à la requête.

Cette méthode prend en paramètre un objet qui contient les critères de recherche.

```js:line-numbers {1}
const User = require('./models/user')
//...
User.findOne({ name: 'John Doe' })
  .then(result => {
    console.log(result)
  })
  .catch(err => {
    console.log(err)
  })
```

## Récupérer un document par son ID

Pour lire un document, on utilise la méthode `findById()`.

`findById()` renvoie le document qui correspond à l'ID passé en paramètre.

```js:line-numbers {1}
const User = require('./models/user')
//...
User.findById('5e9b9b9b9b9b9b9b9b9b9b9b')
  .then(result => {
    console.log(result)
  })
  .catch(err => {
    console.log(err)
  })
```

--- 

Il existe plusieurs autres méthodes pour lire, supprimer et mettre à jour des documents.

- Model.deleteMany()
- Model.deleteOne()
- Model.find()
- Model.findById()
- Model.findByIdAndDelete()
- Model.findByIdAndRemove()
- Model.findByIdAndUpdate()
- Model.findOne()
- Model.findOneAndDelete()
- Model.findOneAndRemove()
- Model.findOneAndReplace()
- Model.findOneAndUpdate()
- Model.replaceOne()
- Model.updateMany()
- Model.updateOne()

Toutes les méthodes sont visibles ici :
- https://mongoosejs.com/docs/queries.html


:::tip Pratique
Copier la démo7 et apportez des modifications afin d'utiliser la base de données pour enregistrer et lire les articles de blogue.
:::

::: tip Conseil
Voir la démo8-mongoose pour voir la correction dans quelques jours…
:::