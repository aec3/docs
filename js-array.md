# Array

Quelques méthodes utiles pour manipuler les tableaux.

## include()

Permet de savoir si un élément est présent dans un tableau.

```js
const tableau = [1, 2, 3, 4, 5];

tableau.includes(3); // true
tableau.includes(6); // false
```


## sort()

Permet de trier un tableau.

```js
const tableau = [1, 5, 4, 2, 3];

tableau.sort(); // [1, 2, 3, 4, 5]
tableau.sort().reverse(); // [5, 4, 3, 2, 1]
```

## forEach()

Permet d'exécuter une fonction sur chaque élément d'un tableau.

```js
const tableau = ["chien", "chat", "oiseau"];

tableau.forEach((element) => {
  console.log(element);
});
```
Vous voulez passer la douane avec vos animaux de compagnie. Vous avez une liste de vos animaux et une liste des animaux autorisés. 

Vous voulez afficher un tableau avec seulement vos animaux autorisés.

```js
const mesAnimaux = ["araignée", "chat", "oiseau", "éléphant"];
const animauxAutorises = ["chat", "oiseau", "chien"];
const mesAnimauxQuiPeuventPasser = [];

mesAnimaux.forEach((element) => {
  if (animauxAutorises.includes(element)) {
    mesAnimauxQuiPeuventPasser.push(element);
  }
});

console.log(mesAnimauxQuiPeuventPasser); // ["chat", "oiseau"]
```

---

Exemple avec un tableau d'objets :

```js
const mesAnimaux = [
  { nom: "araignée", autorise: false },
  { nom: "chat", autorise: true },
  { nom: "oiseau", autorise: true },
  { nom: "éléphant", autorise: false },
];

const mesAnimauxQuiPeuventPasser = [];

mesAnimaux.forEach((element) => {
  if (element.autorise) {
    mesAnimauxQuiPeuventPasser.push(element.nom);
  }
});

console.log(mesAnimauxQuiPeuventPasser); // ["chat", "oiseau"]
```

---

Exemple avec un tableau d'objets et un tableau `loisirs` dans l'objet.

On souhaite afficher un tableau avec tous les loisirs de tous les utilisateurs mais sans doublons.

```js
const utilisateurs = [
  {
    id: 1,
    nom: "Alice",
    loisirs: ["natation", "lecture", "voyages"]
  },
  {
    id: 2,
    nom: "Aminata",
    loisirs: ["musique", "natation"]
  },
  {
    id: 3,
    nom: "Gandalf",
    loisirs: ["escalade", "voyages", "musique"]
  }
];

const loisirs = [];

utilisateurs.forEach((utilisateur) => {
  utilisateur.loisirs.forEach((loisir) => {
    if (!loisirs.includes(loisir)) {
      loisirs.push(loisir);
    }
  });
});

console.log(loisirs); 
// ["natation", "lecture", "voyages", "musique", "escalade"]
```

## push(), pop(), shift(), unshift()

Permettent de manipuler un tableau en ajoutant ou supprimant des éléments.

```js
const tableau = [1, 2, 3, 4, 5];

// Ajoute un élément à la fin du tableau
tableau.push(6); // [1, 2, 3, 4, 5, 6]

// Supprime le dernier élément du tableau
tableau.pop(); // [1, 2, 3, 4, 5]
// On peut récupérer la valeur supprimée
const val1 = tableau.pop(); // [1, 2, 3, 4]
// val1 = 5


// Supprime un élément au début du tableau
tableau.shift(); // [2, 3, 4]
// On peut récupérer la valeur supprimée
const val2 = tableau.shift(); // [3, 4]
// val2 = 2


// Ajoute un élément au début du tableau
tableau.unshift(15); // [15, 3, 4]
```



