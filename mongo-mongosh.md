# Installation de Mongosh

Mongosh est une interface de ligne de commande pour MongoDB. Il est disponible pour Windows, macOS et Linux.

## 1 - Télécharger Mongosh

https://www.mongodb.com/try/download/shell

![HTTP](./img/mongo21.png)

## 2 - Installer Mongosh

Lancez le package d'installation téléchargé.

Suivre les étapes d'installation, il n'y a pas de configuration particulière à faire.

Une fois l'installation terminée, vous pouvez lancer Mongosh en exécutant la commande `mongosh` dans un terminal.

![HTTP](./img/mongo22.png){data-zoomable}


