# Contrôleurs

Afin de respecter le modèle MVC, nous allons créer les contrôleurs de notre application.

Pour cela, nous allons créer un dossier `controllers`. Dans ce dossier, nous pouvons créer un ou plusieurs fichiers qui contiendront nos contrôleurs.

Pour notre démo, nous allons créer un contrôleur `adminController.js` et `postsController.js`.

Nous ajouterons également un contrôleur `errorController.js` qui sera utilisé pour gérer les erreurs.

![Contrôleurs](img/controller1.png)

Dans les fichiers de route, nous allons importer les contrôleurs et les utiliser pour gérer les requêtes.

## Exemple avec le contrôleur `postsController`

Voici le code que nous avions pour la route de la page d'accueil `/` dans le fichier `routes/post.js` :

![Contrôleurs](img/controller2.png)

Nous remplaçons le code par :

![Contrôleurs](img/controller3.png)

- 1. Nous importons le contrôleur `postsController` que nous avons créé.
- 2. Nous utilisons la méthode `getPosts` du contrôleur pour gérer la requête.

Maintenant dans le fichier `controllers/postsController.js`, nous pouvons créer la méthode `getPosts` :

![Contrôleurs](img/controller4.png)

- 1. Nous exportons la méthode `getPosts` pour pouvoir l'utiliser dans le fichier `routes/post.js`.

::: tip Remarque
Il faut donc de copier le code qui se trouve dans les fichiers de route pour le mettre dans les fichiers de contrôleur.
:::

::: tip Exercice
Faites une copie de la démo précédente et créez tous les contrôleurs nécessaires.
:::

## Contrôleur `errorController`

Nous allons créer un contrôleur `errorController.js` qui sera utilisé pour gérer les erreurs.

Voici le code du fichier `controllers/errorController.js` :

![Contrôleurs](img/controller5.png)

- 1. Nous exportons la méthode `get404` pour pouvoir l'utiliser.

Dans le fichier `app.js`, nous allons importer le contrôleur `errorController` et l'utiliser pour gérer les erreurs :

![Contrôleurs](img/controller7.png)

Puis avant le code qui démare le serveur, nous allons ajouter le middleware `errorController.get404` :

![Contrôleurs](img/controller6.png)