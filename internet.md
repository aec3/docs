# Internet
## Comment ça marche ?
Internet est un réseau de réseaux. Il est composé de milliers de réseaux locaux, appelés réseaux privés, qui sont eux-mêmes composés de milliers de serveurs et de milliers de clients. 

## Le Web
Le web est un réseau de serveurs qui communiquent entre eux. Chaque serveur est relié à un autre serveur par un câble ou une liaison sans fil. Les serveurs sont reliés entre eux par des liaisons de plus en plus rapides, ce qui permet de faire circuler de plus en plus d'informations.

<!-- insert image -->
![An image](./img/2023-03-08_00-26.png)

