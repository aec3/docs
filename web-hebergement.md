# Hébergement

## CORS : Cross-Origin Resource Sharing

![CORS](/img/sw20.png){ data-zoomable }

Le CORS est un mécanisme de sécurité qui permet à un serveur d'indiquer à un navigateur si une requête provenant d'une origine donnée est autorisée à accéder à une ressource ou non.

Dans Express.js, il est possible d'utiliser un middleware pour gérer le CORS. Il faut l'ajouter avant les routes :

L'étoile `*` permet d'autoriser toutes les origines. Il est possible de spécifier une origine particulière, par exemple `https://monsupersite.com`.

```js{2}
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  // res.setHeader('Access-Control-Allow-Origin', 'https://monsupersite.com');

  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});
```
Si le CORS n'est pas configuré correctement, vous pourriez avoir une erreur de ce type dans la console du navigateur :

![CORS](/img/sw21.png){ data-zoomable }

## Requête de pré-vérification

- Une requête de pré-vérification cross-origin CORS est une requête de vérification faite pour contrôler si le protocole CORS est autorisé.
- C'est une requête utilisant la méthode OPTIONS qui utilise trois en-têtes HTTP : La méthode Access-Control-Request-Method, les en-têtes Access-Control-Request-Headers et Origin.
- Une requête de pré-vérification est automatiquement envoyée par le navigateur quand cela est nécessaire.
- C’est pour cela qu’on ajoute dans le middleware cette partie :

```js
res.setHeader(
  'Access-Control-Allow-Methods',
  'OPTIONS, GET, POST, PUT, PATCH, DELETE'
);
res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
```

https://developer.mozilla.org/fr/docs/Glossary/Preflight_request


## Déploiement sur render.com

- Nous allons utiliser les services de render.com pour mettre notre API en ligne.
- Il existe beaucoup d’entreprise qui propose des serveurs afin d’héberger des API, la plupart sont payantes.
- Render propose un service gratuit pour les applications avec certaines limites.

- Nous allons voir comment passer d’un environnement local à un environnement en production.

### Création d’un compte

Créer un compte gratuit chez render.com.
Créer un nouveau Web Service.

![CORS](/img/sw22.png){ data-zoomable }




### Création d’un nouveau dépôt

Connectez votre compte Gitlab ou Github

![CORS](/img/sw23.png){ data-zoomable }

Puis choisir le dépôt de votre API

![CORS](/img/sw24.png){ data-zoomable }

Complétez le formulaire

![CORS](/img/sw25.png){ data-zoomable }

### Configuration du fichier .env

Dans les options Advanced, choisir « Add Secret File » pour ajouter un fichier .env

![CORS](/img/sw26.png){ data-zoomable }

Saisissez les informations que vous utilisez dans votre fichier .env

Ceci est un exemple que vous devez adapter suivant votre projet.

Vous pourrez modifier ces informations plus tard à partir du Dashboard.

Ne pas mettre le PORT qui sera définit automatiquement.

![CORS](/img/sw27.png){ data-zoomable }

### Dashboard

Le dashboard s’affiche, surveillez
les logs pendant le déploiement
de l’application.
Et vous pouvez aller prendre
un café…

Le service gratuit est lent…

Pour vous inciter à prendre le 
service payant 💲

![CORS](/img/sw28.png){ data-zoomable }

---

![CORS](/img/sw29.png){ data-zoomable }

### Déploiement

L’API est automatiquement déployée quand un push est détecté sur Gitlab ou Github. Vous n’avez donc plus rien à faire

![CORS](/img/sw30.png){ data-zoomable }




