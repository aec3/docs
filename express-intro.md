# Express.js

## Introduction

[Site Web de Express.js](https://expressjs.com/fr/)

Express.js est un framework de développement web pour Node.js. Il fournit un ensemble de fonctionnalités et d'outils pour créer des applications web en utilisant Node.js de manière simple et efficace. Express.js simplifie la création de serveurs HTTP en fournissant des méthodes pour gérer les requêtes HTTP et les réponses.

Express.js est open source et dispose d'une communauté active de développeurs qui fournissent de nombreuses ressources, y compris des bibliothèques et des plugins pour faciliter la création d'applications web robustes et performantes.

## Installation

Pour installer Express.js dans une application, il faut exécuter la commande suivante dans un terminal :

```bash
npm install --save express
```

`--save` permet d'ajouter Express.js à la liste des dépendances de l'application dans le fichier `package.json`.

## Créer une application Express.js

Pour créer une application Express.js, il faut importer le module `express` et d'utiliser la méthode `express()` pour créer une application Express.js.

```js
const express = require('express');
const app = express();
```

La première ligne charge le module Express.js en tant que dépendance de l'application. La variable `express` est donc une référence au module Express.js.

Ensuite, le code crée une instance de l'application Express.js en appelant simplement la fonction `express()`.

Cela crée un objet `app` qui représente notre application Express.js.

## Créer un serveur Express.js

Pour créer un serveur Express.js, il faut utiliser la méthode `listen()` de l'objet `app` et spécifier le port sur lequel le serveur doit écouter.

```js {4-6}
const express = require('express');
const app = express();

app.listen(3000, () => {
  console.log('Le serveur écoute sur le port 3000');
});
```



