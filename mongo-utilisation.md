# MongoDb
## Structure
MongoDb est une base de données orientée document. 
- Une **base de données** est composée de **collections**.
- Une **collection** est composée de **documents**.

![HTTP](./img/mongo23.png)

Chaque document est stocké sous forme de structure de données BSON (Binary JSON), qui est une représentation binaire de la notation JSON.
Très semblables aux fichiers JSON, ces documents sont composés de paires **clé/valeurs**  de plusieurs types. 

[Cliquez ici](https://www.mongodb.com/docs/manual/reference/bson-types/) pour voir les types de données acceptés dans un fichier BSON 


```javascript
// Exemple de document BSON
{ 
  "name": "Aminata", 
  "age": 22, 
  "city": "Québec",
  "group": ["student", "developer"], 
  "birthday": new Date("2000-11-28"),
}

```

![Collection](./img/mongo24.png)
[source](https://www.koderhq.com/tutorial/mongodb/collection/)

## Commandes de base

### Voir les bases de données

Dans mongosh, pour voir les bases de données, tapez la commande suivante.

```javascript
show dbs
```

![show dbs](./img/mongo25.png)

### Utiliser une base de données

Pour utiliser une BD, tapez la commande `use`. 

Si la BD n’existe pas, MongoDB la créera pour vous !

```javascript
use nom_de_la_base_de_donnees
```

![use](./img/mongo26.png)

Si on fait un `show dbs`, on ne voit pas la base de données que l'on vient de créer.

Pour quelle soit effectivement créée, il faudra commencer par y insérer un document.

---
### Supprimer une base de données

Sélectionnez la BD avec la commande `use`.

Une fois sélectionnée, la commande suivante supprime la BD.

```javascript
db.dropDatabase()
```

![dropDatabase](./img/mongo27.png)

## Insérer des documents

### Insérer un document

Pour insérer un document dans une collection, on utilise la méthode `db.collection.insertOne()`

La collection est créée automatiquement si elle n'existe pas.

```javascript
db.etudiant.insertOne({nom: "Aminata", age: 22, city: "Québec"})
```

Si l'id n'est pas spécifié, MongoDB génère un id unique pour chaque document.

![insertOne](./img/mongo28.png){data-zoomable}

### Insertion multiple

Pour insérer plusieurs documents, on utilise la méthode `db.collection.insertMany()`

```javascript
db.etudiant.insertMany([
  {nom: "Sacha", age: 19, city: "Québec"},
  {nom: "Nina", age: 17, city: "Québec"},
  {nom: "Aminata", age: 22, city: "Bordeaux"}
])
```

![insertMany](./img/mongo32.png)

### Voir les documents

Pour voir les documents d'une collection, on utilise la méthode `db.collection.find()`

```javascript
db.etudiant.find()
```

![find](./img/mongo30.png)

On peut voir qu'un  `_id` de type [ObjectId](https://www.mongodb.com/docs/manual/reference/method/ObjectId/) est généré automatiquement par MongoDB. C'est un identifiant **unique** pour chaque document.

Il est de la forme `ObjectId("643c69da3097c72782714459")`


## Manipuler des collections

### Voir les collections

```javascript
show collections
```

![show collections](./img/mongo29.png)


### Supprimer une collection

```javascript
db.collection.drop()
```

![drop](./img/mongo31.png)

