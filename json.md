# JSON
JavaScript Object Notation

JSON est un standard qui facilite l'échange structuré de données entre tous les langages de programmation.

 
Créé par [Douglas Crockford](https://fr.wikipedia.org/wiki/Douglas_Crockford) entre 2002 et 2005, il est décrit par la [RFC 7159](https://tools.ietf.org/html/rfc7159) de l’[IETF](https://fr.wikipedia.org/wiki/Internet_Engineering_Task_Force)

Bien qu'il soit issu du langage JavaScript, il est indépendant du langage de programmation. Sa structure étant normalisée, cela permet à n'importe quel langage de programmation capable de le décoder de lire les informations.

Il permet de stocker et d'échanger des informations.

Un fichier au format JSON a pour extension `.json`.

## Syntaxe

Un document JSON peut être :

- Un objet {...}
```js
{
   "nom" : "Gandalf",
   "prenom" : "Le Gris",
   "espèce" : "Maia"
}
```
- Un tableau [...]

```js
[
  "Gandalf",
  "Le Gris",
  "Maia"
]
```
Le séparateur utilisé est la virgule.

Deux types d'éléments :

- des couples de type "nom": valeur
- des listes de valeurs, comme les tableaux.

### Valeurs possibles

- Primitifs : nombre, booléen, chaîne de caractères, null ;

- Tableaux : Liste de valeurs (tableaux et objets aussi autorisés) ;

- Objets : Listes de couples "nom": valeur.


### Exemple
Trouver les deux erreurs

```js
{
  "liste_messages": [
    {
      "objet": "Fuyez !",
      "destinataire": "Freudon",
      "expediteur": "Gandalf",
      "date": "21/07/2941",
      "contenu": "Fuyez, pauvres fous !",
      "important": "oui",
      "categories": [
        {
          "nom": "personnel",
        }
      ]
    }
    {
      "objet": "Bien reçu",
      "destinataire": "Gandalf",
      "expediteur": "Freudon",
      "date": "21/07/2941",
      "contenu": "Ok, on y va, tu nous rejoins plus tard !",
      "important": "non",
      "categories": [
        {
          "nom": "personnel"
        },
        {
          "nom": "planification"
        }
      ]
    }
  ]
}
```

### Exemple d'utilisation en Javascript

```js
var img = {
  "Image": {
      "Width":  800,
      "Height": 600,
      "Title":  "View from 15th Floor",
      "Thumbnail": {
          "Url":    "http://www.example.com/image/481989943",
          "Height": 125,
          "Width":  100
      },
      "Animated" : false,
      "IDs": [116, 943, 234, 38793]
    }
}
```



Si on met cet objet dans une variable img, on peut accéder à la valeur Height en utilisant:
```js
img.Image.Height // 600
```

Pour trouver la première valeur de IDs
```js
img.Image.IDs[0] // 116
```

### Autre exemple Javascript

Ce JSON contient deux objets

```js
var cities = [
  {
      "precision": "zip",
      "Latitude":  37.7668,
      "Longitude": -122.3959,
      "Address":   "",
      "City":      "SAN FRANCISCO",
      "State":     "CA",
      "Zip":       "94107",
      "Country":   "US"
  },
  {
      "precision": "zip",
      "Latitude":  37.371991,
      "Longitude": -122.026020,
      "Address":   "",
      "City":      "SUNNYVALE",
      "State":     "CA",
      "Zip":       "94085",
      "Country":   "US"
  }
]
```

Pour accéder au nom de la première ville
```js
cities[0].City // SAN FRANCISCO
```

Pour accéder à la longitude de la deuxième ville
```js
cities[1].Longitude // -122.026020
```

## Fonctions utiles en JavaScript

`parse()`: converti JSON en JavaScript

`stringify()`: converti JavaScript en JSON

```js
var magicien = '{"nom": "Gandalf", "prenom": "Le Gris", "espèce": "Maia"}'

var personnage = JSON.parse(magicien)

console.log(personnage) // Object { nom: "Gandalf", prenom: "Le Gris", "espèce": "Maia" }

console.log(personnage.nom) // "Gandalf" 

console.log(JSON.stringify(personnage)) // {"nom":"Gandalf","prenom":"Le Gris","espèce":"Maia"}
```

## Exemples d'API utilisant JSON

API WordPress
- [https://nature-humaine.ca/wp-json/wp/v2/posts/](https://nature-humaine.ca/wp-json/wp/v2/posts/)

jsonplaceholder
- [https://jsonplaceholder.typicode.com/posts](https://jsonplaceholder.typicode.com/posts)

## Outils

Convertisseurs json / csv

- [https://json-csv.com](https://json-csv.com)

Parser en ligne

- [https://jsoneditoronline.org](https://jsoneditoronline.org)
