import { SearchPlugin } from "vitepress-plugin-search";
import { defineConfig } from 'vitepress'

var options = {
  previewLength: 62,
  buttonLabel: "Recherche",
  placeholder: "Recherche docs",
  language: "fr",
  allow: [],
  ignore: [],
};

// https://vitepress.vuejs.org/reference/site-config
export default defineConfig({
  title: "AEC développement Web",
  description: "Node.js",
  // outDir: '../public',
  base: "/docs/",
  themeConfig: {
    // https://vitepress.vuejs.org/reference/default-theme-config
    aside: true,
    nav: [
      { text: 'Accueil', link: '/' },
      // { text: 'Examples', link: '/markdown-examples' }
    ],
    sidebar: [
      {
        text: 'Généralités',
        collapsed: false,
        items: [
          { text: 'Le DOM', link: '/dom' },
          { text: 'Protocole HTTP', link: '/http' },
          { text: 'JSON', link: '/json' },
          { text: 'Modèle MVC', link: '/mvc' },
          
          // { text: 'Internet', link: '/internet' }
        ],
      },
      {
        text: 'Express.js',
        collapsed: false,
        items: [
          { text: 'Intro', link: '/express-intro' },
          { text: 'Routes', link: '/express-routes' },
          { text: 'Middleware', link: '/express-middleware' },
          { text: 'Templates', link: '/express-templates' },
          { text: 'Fichiers statiques', link: '/express-static' },
          { text: 'Router', link: '/express-router' },
          { text: 'Courriel', link: '/express-email' },
          { text: 'Mongoose', link: '/express-mongoose' },
          { text: 'Contrôleurs', link: '/express-controller' },
          { text: 'Gestion des erreurs', link: '/express-erreurs' },
        ]
      },
      {
        text: 'mongoDB',
        collapsed: false,
        items: [
          { text: 'Intro', link: '/mongo-intro' },
          { text: 'Installation', link: '/mongo-install' },
          { text: 'Configuration', link: '/mongo-config' },
          { text: 'Mongosh', link: '/mongo-mongosh' },
          { text: 'Utilisation', link: '/mongo-utilisation' },
          { text: 'Sélection', link: '/mongo-select' },
          { text: 'Mise à jour / Suppression', link: '/mongo-update-delete' },
          { text: 'MongoDB Atlas', link: '/mongo-atlas' },
        ]
      },
      {
        text: 'Service Web',
        collapsed: false,
        items: [
          { text: 'Intro', link: '/web-service-intro' },
          { text: 'Postman', link: '/web-service-postman' },
          { text: 'API avec Express', link: '/web-service-api' },
          { text: 'Postman scripts & variables', link: '/web-service-postman-variable' },
          { text: 'Authentification JWT', link: '/jwt' },
          { text: 'Hashage des mots de passe', link: '/bcrypt' },
          { text: 'Hébergement', link: '/web-hebergement' },


        ]
      },
      {
        text: 'javascript',
        collapsed: false,
        items: [
          { text: 'Array', link: '/js-array' },
          { text: 'If ternaire', link: '/js-if-ternaire' },
        ]
      },
    ],
    footer: {
      message: '.',
      copyright: ''
    },
    returnToTopLabel: 'Retour en haut',
    socialLinks: [
      // { icon: 'github', link: 'https://gitlab.com/aec3' }
    ]
  },
  vite: { plugins: [SearchPlugin(options)] }
})
