# bcrypt

La librairie `bcrypt` permet de hasher les mots de passe.

Le hashage est un procédé de cryptographie qui permet de transformer une chaîne de caractères en une autre chaîne de caractères, appelée hash. Le hashage est une opération à sens unique, c'est-à-dire qu'il est impossible de retrouver la chaîne de caractères d'origine à partir du hash.

## Installation

```js
npm install --save bcryptjs
```

## Exemple d'implémentation

La méthode `hash()` permet de hasher une chaîne de caractères. Elle prend en paramètre la chaîne de caractères à hasher et le nombre de tours de l'algorithme de hashage.

```js
const bcrypt = require('bcryptjs');

//...
// Au moment de la création d'un utilisateur

// Hashage du mot de passe
bcrypt.hash(password, 10)
  .then(hash => {
    //...
  })
  .catch(error => {
    //...
  });
```

## Comparaison de mots de passe

```js
const bcrypt = require('bcryptjs');

//...
// Au moment de la connexion de l'utilisateur

// Comparaison du mot de passe
bcrypt.compare(password, user.password)
  .then(valid => {
    //...
  })
  .catch(error => {
    //...
  });
```
