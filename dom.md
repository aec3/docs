# Le DOM
## Qu'est-ce que le DOM ?

Ce n'est pas votre code HTML, même si c'est une idée fausse courante. 

Le DOM (Document Object Model) est une [API](https://developer.mozilla.org/fr/docs/Learn/JavaScript/Client-side_web_APIs/Introduction) qui représente le document chargé (c'est-à-dire le site web).

C'est une sorte de pont entre le HTML et le code JavaScript.

Grâce au DOM, les développeurs peuvent manipuler les éléments HTML et CSS d'une page, ajouter ou supprimer des éléments, modifier les styles, et réagir aux événements déclenchés par les utilisateurs.

Sans l'API DOM, vous ne pourriez pas écouter les événements ou modifier l'apparence de la page web chargée avec JavaScript.

Le DOM est une représentation logique de la page web chargée dans le navigateur. Il se présente sous la forme d'un arbre, où chaque élément HTML est représenté par un nœud, et chaque nœud peut avoir des nœuds enfants. Pour accéder au DOM, nous utilisons l'objet global `document`.

L'API DOM fournit un accès programmable à cette structure d'arbre. Elle offre également des méthodes pour créer, éditer et supprimer des éléments de la page web chargée. L'API DOM est donc nécessaire pour interagir avec la page web et son contenu en utilisant JavaScript. C'est pourquoi on parle de l'API DOM comme étant un pont entre JavaScript et HTML.



 ![Exemple de DOM](./img/DOM-model.svg){data-zoomable}
Par ‍Birger Eriksson — Travail personnel, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=18034500

## Sélectionner des éléments du DOM

Pour sélectionner des éléments du DOM, nous utilisons les méthodes `querySelector()` et `querySelectorAll()`. La méthode `querySelector()` renvoie le premier élément qui correspond au sélecteur CSS donné, tandis que `querySelectorAll()` renvoie tous les éléments correspondant au sélecteur CSS.

```js
// sélectionne le premier élément <h1>
const header = document.querySelector('h1');

// sélectionne tous les éléments <p>
const paragraphs = document.querySelectorAll('p');
```


## Accéder et modifier le contenu des éléments

Pour accéder et modifier le contenu d'un élément, nous utilisons les propriétés `textContent` et `innerHTML`. La propriété `textContent` renvoie le contenu textuel de l'élément, tandis que `innerHTML `renvoie le contenu HTML de l'élément, y compris les balises.

```js
// accède au contenu textuel d'un élément <p>
const paragraphText = document.querySelector('p').textContent;

// modifie le contenu HTML d'un élément <div>
document.querySelector('div').innerHTML = '<h2>Nouveau titre</h2>';
```

## Ajouter et supprimer des éléments

Pour ajouter un nouvel élément au DOM, nous utilisons la méthode `createElement()`, qui crée un nouvel élément vide. Nous pouvons ensuite ajouter du contenu à l'élément à l'aide des propriétés `textContent` et `innerHTML`, puis ajouter l'élément au DOM à l'aide de la méthode`appendChild()`.

```js
// crée un nouvel élément <p>
const newParagraph = document.createElement('p');

// ajoute du texte à l'élément
newParagraph.textContent = 'Nouveau paragraphe ajouté !';

// ajoute l'élément au DOM
document.querySelector('body').appendChild(newParagraph);
```

Pour supprimer un élément du DOM, nous pouvons utiliser la méthode `remove()`.

```js
// sélectionne l'élément <p> à supprimer
const paragraphToRemove = document.querySelector('p');

// supprime l'élément
paragraphToRemove.remove();
```

## Écouter les événements

Le DOM permet également d'écouter les événements, tels que les clics de souris ou les pressions de touche du clavier. Nous utilisons la méthode `addEventListener()` pour écouter les événements et exécuter une fonction de rappel en réponse à l'événement.
La méthode `removeEventListener()` permet de supprimer un gestionnaire d'événement.

```js
const btn = document.getElementById('my-button');

function handleClick() {
  console.log('Button clicked!');
}

btn.addEventListener('click', handleClick);

// Plus tard dans le code, nous décidons de supprimer l'événement :
btn.removeEventListener('click', handleClick);
```

## En savoir plus

- [MDN Web Docs DOM](https://developer.mozilla.org/fr/docs/Web/API/Document_Object_Model/Introduction)