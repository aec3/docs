# Envoyer des courriel avec Express.js

## Installation du module nodemailer

Pour envoyer des courriels avec Express.js, nous allons utiliser le module [nodemailer](https://nodemailer.com/about/).

Pour installer le module `nodemailer`, il faut exécuter la commande suivante :

```bash
npm install --save nodemailer
```

## Configuration du serveur SMTP avec Mailtrap

Pour envoyer des courriels, il faut configurer un serveur SMTP. Un serveur SMTP est un serveur qui permet d'envoyer des courriels.

Pour faire des tests, nous allons utiliser [Mailtrap](https://mailtrap.io/). C'est un service qui permet de faire des tests d'envoi de courriel et on peut ensuite voir les courriels qui ont été envoyés.

Pour utiliser Mailtrap, il faut créer un compte sur le site [Mailtrap.io](https://mailtrap.io/).

Après avoir créé un compte, il faut créer un nouveau projet.


![Mailtrap](./img/email1.png)

---
👉 Cliquer sur le bouton `Add project`.
![Mailtrap](./img/email2.png)

---

👉 Choisir le nom du projet

![Mailtrap](./img/email3.png)

---
👉 Cliquer sur le bouton `Add inbox`.
![Mailtrap](./img/email4.png)

---

👉 Choisir le nom de l'*inbox*
![Mailtrap](./img/email5.png)

---

👉 Cliquer sur la *inbox* pour voir les informations de configuration du serveur SMTP.

![Mailtrap](./img/email6.png)

---

- 1- Sélectionner **Nodemailer** dans le sélecteur **Integration**.
- 2- Le code de configuration du serveur SMTP est affiché.

![Mailtrap](./img/email7.png)

## Configuration du serveur SMTP dans Express.js

Pour configurer le serveur SMTP dans Express.js, il faut créer un fichier `nodemailer.js` dans le dossier `config` et y copier le code de configuration du serveur SMTP.

```js
// Configuration du serveur SMTP
const nodemailer = require('nodemailer');

// Configuration du serveur SMTP copié depuis Mailtrap
var transport = nodemailer.createTransport({
  host: "sandbox.smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "5caf1c6ec5077c",
    pass: "637d1c2b82002f"
  }
});

// sendEmail sera la fonction qui permet d'envoyer un courriel
// On pourra l'appeler à partir d'un autre fichier quand on aura besoin d'envoyer un courriel
const sendEmail = (title, content) => {
    transport.sendMail({
      from: mail_sender,
      to: "test@test.ca",
      subject: "Vous avez un message",
      html: `<h1>Message</h1>
          <h2>${title}</h2>
          <p>${content}</p>`,
    }).catch(err => console.log(err));
  };

// Exporter le module pour pouvoir l'utiliser dans d'autres fichiers.
exports.sendEmail = sendEmail;
```

::: tip Information
On peut également ajouter d'autres fonctions dans ce fichier et les exporter pour pouvoir envoyer d'autres types de courriels.
:::

## Créer une route pour envoyer un courriel

Il faut maintenant ajouter une route dans un fichier de route pour envoyer un courriel.

Exemple :

```js
const express = require('express');
const router = express.Router();
// Importe la fonction sendEmail du fichier nodemailer.js
const { sendEmail } = require("../config/nodemailer");

// Reçoit les données d'un formulaire de contact
// avec un titre et un contenu
router.post('/message', (req, res, next) => {
  console.log(req.body);
  const { title, content } = req.body;

  // Envoie un courriel avec le titre et le contenu
  sendEmail(title, content);

  // Redirige vers la page d'accueil
  res.redirect('/');
});
```

## Voir les messages envoyés dans Mailtrap

Pour voir les messages envoyés dans Mailtrap, il faut aller sur le site [Mailtrap](https://mailtrap.io/) et cliquer sur le bouton `Inbox` dans le projet créé.

![Mailtrap](./img/email8.png)

🥳🥳🥳