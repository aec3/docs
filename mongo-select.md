

# Sélectionner des documents (requêtes)

## Sélectionner tous les documents

Pour retourner tous les documents d'une collection, on utilise la méthode `db.collection.find()`

```javascript
db.etudiant.find()
```

![find](./img/mongo33.png)

On peut ajouter `.pretty()` à la fin de la requête pour afficher les documents de façon plus lisible.

```javascript
db.etudiant.find().pretty()
```


## Sélection, critères et projection

La fonction `find()` peut prendre deux paramètres : 

`find(critères, projection)`

- Les critères (obligatoire) vont permettre de sélectionner les documents qui correspondent à la requête.
- La projection (optionnelle) va permettre de sélectionner les champs à afficher.

On spécifie les champs de la façon suivante : `{champ1: <valeur>, champ2:<valeur>}`.

La valeur peut être :
-	1 ou true pour inclure le champ
-	0 ou false pour exclure le champ

### Exemple critère

```javascript
db.etudiant.find({nom: "Aminata"})
```

Cette requête va retourner tous les documents dont le champ `nom` est égal à `Aminata` (il peut y en avoir plusieurs).

![find](./img/mongo34.png)

Si on souhaite sélectionner les documents dont le nom commence par `A`, on peut utiliser les expressions régulières en utilisant les caractères `/` comme délimiteur.

```javascript
db.etudiant.find({nom: /^A/})
```

Le caractère `^` indique que la chaîne doit commencer par `A`.

### Exemple critère et projection

```javascript
db.etudiant.find({city: "Québec"}, {nom: 1, age: 1})
```

Cette requête va retourner tous les documents dont le champ `city` est égal à `Québec` et qui ne contiennent que les champs `nom` et `age`.

![find](./img/mongo35.png)

Il y a seulement deux documents qui correspondent à la requête.

On peut voir que le champ `_id` est toujours présent, même si on ne l'a pas spécifié dans la projection.

Pour ne pas l'afficher, on peut utiliser la valeur `0` ou `false` pour le champ `_id`.

```javascript
db.etudiant.find({city: "Québec"}, {nom: 1, age: 1, _id: 0})
```

![find](./img/mongo36.png)

## Opérateurs logiques

Les opérateurs logiques permettent de combiner plusieurs critères.

### Opérateur `$and`

L'opérateur `$and` permet de combiner plusieurs critères avec un **ET** logique.

```javascript
db.etudiant.find({$and: [{nom: "Aminata"}, {age: 22}]})
```

Cette requête va retourner tous les documents dont le champ `nom` est égal à `Aminata` **et** le champ `age` est égal à `22`.

Cependant on peut utiliser l'opérateur `$and` de façon implicite en spécifiant plusieurs critères séparés par une virgule.

```javascript
db.etudiant.find({nom: "Aminata", age: 22})
```

---

### Opérateur `$or`

L'opérateur `$or` permet de combiner plusieurs critères avec un OU logique.

```javascript
db.etudiant.find({$or: [{nom: "Aminata"}, {age: 22}]})
```

Cette requête va retourner tous les documents dont le champ `nom` est égal à `Aminata` **ou** le champ `age` est égal à `22`.

---

### Opérateur `$not`

L'opérateur `$not` permet de combiner plusieurs critères avec un **NON** logique.

```javascript
db.etudiant.find({nom: {$not: {$eq: "Aminata"}}})
```

Cette requête va retourner tous les documents dont le champ `nom` est **différent** de `Aminata`.

---

### Combinaison des opérateurs

On peut combiner les opérateurs `$and`, `$or` et `$not` pour créer des requêtes complexes.

```javascript
db.etudiant.find({
  nom: {
    $not: {
      $eq: "Aminata"
    }
  },
  age: {
    $gte: 17
  }
})
```

Cette requête va retourner tous les documents dont le champ `nom` est **différent** de `Aminata` **et** le champ `age` est **supérieur ou égal** à `17`.


## Opérateurs de comparaison

Les opérateurs de comparaison permettent de sélectionner des documents en fonction de leur valeur.

Liste de tous les opérateurs de comparaison : https://docs.mongodb.com/manual/reference/operator/query-comparison/


### Opérateur `$eq`

*(Equal => égal à)*

L'opérateur `$eq` permet de sélectionner les documents dont la valeur d'un champ est égale à une valeur donnée.

```javascript
db.etudiant.find({age: {$eq: 22}})
```


### Opérateur `$gt`

*(Greater Than => supérieur à)*

L'opérateur `$gt` permet de sélectionner les documents dont la valeur d'un champ est **supérieure** à une valeur donnée.

```javascript
db.etudiant.find({age: {$gt: 19}})
```

### Opérateur `$gte`

*(Greater Than or Equal => supérieur ou égal à)*

L'opérateur `$gte` permet de sélectionner les documents dont la valeur d'un champ est **supérieure ou égale** à une valeur donnée.

```javascript
db.etudiant.find({age: {$gte: 19}})
```

### Opérateur `$lt`

*(Less Than => inférieur à)*

L'opérateur `$lt` permet de sélectionner les documents dont la valeur d'un champ est **inférieure** à une valeur donnée.

```javascript
db.etudiant.find({age: {$lt: 19}})
```

### Opérateur `$lte`

*(Less Than or Equal => inférieur ou égal à)*

L'opérateur `$lte` permet de sélectionner les documents dont la valeur d'un champ est **inférieure ou égale** à une valeur donnée.

```javascript 
db.etudiant.find({age: {$lte: 19}})
```

### Opérateur `$ne`


*(Not Equal => différent de)*

L'opérateur `$ne` permet de sélectionner les documents dont la valeur d'un champ est **différente** d'une valeur donnée.

```javascript
db.etudiant.find({age: {$ne: 19}})
```

### Opérateur `$in`


*(In => dans)*

L'opérateur `$in` permet de sélectionner les documents dont la valeur d'un champ est **dans** un tableau de valeurs.

```javascript
db.etudiant.find({age: {$in: [19, 20, 21]}})
```

### opérateur `$all`

*(All => tous)*

L'opérateur `$all` permet de sélectionner les documents dont la valeur d'un champ est **tous** les éléments d'un tableau de valeurs.

```javascript
db.etudiant.find({tags: {$all: ["informatique", "programmation"]}})
```

## Trier les résultats

La méthode `find()` permet de trier les résultats avec la méthode `sort()`.

```javascript
db.etudiant.find().sort({nom: 1})
```

Cette requête va retourner tous les documents de la collection `etudiant` triés par ordre alphabétique croissant sur le champ `nom`.

Pour trier par ordre alphabétique décroissant, il faut utiliser la valeur `-1` :

```javascript
db.etudiant.find().sort({nom: -1})
```

## Limiter les résultats

La méthode `find()` permet de limiter les résultats avec la méthode `limit()`.

```javascript
db.etudiant.find().limit(2)
```

Cette requête va retourner les 2 premiers documents de la collection `etudiant`.

## Compter les résultats

La méthode `find()` permet de compter les résultats avec la méthode `count()`.

```javascript
db.etudiant.find().count()
```

Cette requête va retourner le nombre de documents de la collection `etudiant`.


## Exercice

::: tip Exercice

Dans MongoSh, placer vous dans une base de données, par exemple `cegep` avec la commande `use cegep`.

Puis copier-coller le code suivant pour créer la collection `products` :

```javascript

db.products.insertMany([{ 
  _id: ObjectId("61177ed92f7357f1d1914d4a"),
  nom: "Ordinateur portable",
  prix: 999.99,
  en_stock: true,
  quantite: 10,
  tags: ["informatique", "portable"]
},
{ 
  _id: ObjectId("61177ee52f7357f1d1914d4b"),
  nom: "Souris sans fil",
  prix: 29.99,
  en_stock: true,
  quantite: 50,
  tags: ["informatique", "accessoire"]
},
{ 
  _id: ObjectId("61177ef22f7357f1d1914d4c"),
  nom: "Tapis de souris",
  prix: 9.99,
  en_stock: false,
  quantite: 0,
  tags: ["informatique", "accessoire"]
}])

```

Ecrire les requêtes qui permettent de résoudre les problèmes suivants :

- 1. Afficher tous les documents de la collection `products`.
- 2. Afficher tous les documents dont le champ `prix` est égal à `29.99`.
- 3. Afficher tous les documents dont le champ `prix` est supérieur ou égal à `29.99`.
- 4. Récupérez tous les documents où le champ `en_stock` est `true` et le champ `quantite` est supérieur à 0.
- 5. Récupérez tous les documents où le champ `tags` contient à la fois "informatique" et "accessoire".
- 6. Récupérez tous les documents où le champ `prix` est inférieur à 50 ou le champ `quantite` est supérieur ou égal à 50.
- 7. Récupérez tous les documents où le champ `prix` n'est pas égal à 9.99.
- 8. Récupérez tous les documents où le champ nom commence par la lettre "O".

:::

## Solutions

- 1. Afficher tous les documents de la collection `products`.
```javascript
db.products.find()
```

- 2. Afficher tous les documents dont le champ `prix` est égal à `29.99`.
```javascript
db.products.find({ prix: 29.99 })
```

- 3. Afficher tous les documents dont le champ `prix` est supérieur ou égal à `29.99`.
```javascript
db.products.find({ prix: { $gte: 29.99 } })
```

- 4. Récupérez tous les documents où le champ en_stock est true et le champ quantite est supérieur à 0.
```javascript
db.products.find({ en_stock: true, quantite: { $gt: 0 } })
```

- 5. Récupérez tous les documents où le champ tags contient à la fois "informatique" et "accessoire".
```javascript
db.products.find({ tags: { $all: ["informatique", "accessoire"] } })
```
- 6. Récupérez tous les documents où le champ prix est inférieur à 50 ou le champ quantite est supérieur ou égal à 50.
```javascript
db.products.find({ $or: [{ prix: { $lt: 50 } }, { quantite: { $gte: 50 } }] })
```

- 7. Récupérez tous les documents où le champ prix n'est pas égal à 9.99.
```javascript
db.products.find({ prix: { $ne: 9.99 } })
```

- 8. Récupérez tous les documents où le champ nom commence par la lettre "O".
```javascript
db.products.find({ nom: /^O/ })
```

Notez que pour chacune de ces commandes, vous pouvez ajouter un `pretty()` à la fin pour afficher les résultats de manière plus lisible.

Par exemple :

```javascript
db.products.find({ en_stock: true, quantite: { $gt: 0 } }).pretty()
``` 