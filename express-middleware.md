# Middleware

Le concept principal de Express.js est le middleware. C'est un ensemble de fonctions que chaque requête entrante traverse automatiquement, permettant de diviser le code en plusieurs blocs plutôt que d'avoir une seule fonction qui fait tout. 

Ainsi il est facile d'ajouter d'autres packages tiers qui fournissent des fonctions middleware supplémentaires pour ajouter des fonctionnalités spécifiques.

Pour utiliser le middleware, on peut utiliser la méthode use qui permet d'ajouter une nouvelle fonction middleware à l'application Express.js. Cette fonction sera exécutée pour chaque requête entrante et reçoit trois arguments : 
- la requête, 
- la réponse 
- une fonction next qui doit être appelée pour permettre à la requête de passer au middleware suivant.

![HTTP](./img/middleware1.png){data-zoomable}

Les middleware permettent de faire des choses avant ou après une requête. Par exemple, on peut vérifier si l'utilisateur est connecté avant de lui montrer une page, ou on peut vérifier si l'utilisateur a le droit d'accéder à une page avant de lui montrer.

## Les middlewares globaux

Les middlewares globaux sont des middlewares qui s'appliquent à toutes les routes. Ils sont définis avec la méthode `app.use()`.

```js
app.use((req, res, next) => {
  console.log('Je suis un middleware global')
  next()
})
```

Ce code sera exécuté pour chaque requête entrante, quelque soit la route et la méthode HTTP utilisée.

## Les middlewares globaux avec une route

Les middlewares globaux avec une route sont des middlewares qui s'appliquent à une route spécifique. Ils sont définis avec la méthode `app.use('/path')`.

```js
app.use('/users', (req, res, next) => {
  console.log('Je suis un middleware global avec une route')
  next()
})
```

Dans cet exemple, le middleware sera exécuté uniquement pour les requêtes qui ont un chemin qui **commence** par `/users`.

::: warning Attention
Les middlewares globaux avec une route sont exécutés avant les middlewares de routage.

En règle générale, on evite d'utiliser les middlewares globaux avec une route. On préfère utiliser les middlewares de routage car on n'a pas besoin de faire attention à l'ordre dans lequel on les ajoute.
:::


## Les middlewares de routage

Les middlewares de routage sont des middlewares qui s'appliquent à une route spécifique. Ils sont définis avec la méthode `app.METHOD()`.

Nous les avons déjà vus dans le chapitre sur les routes.

```js
app.get('/users', (req, res, next) => {
  console.log('Je suis un middleware de routage')
  next()
})
```

## Les middlewares de gestion d'erreur

Les middlewares de gestion d'erreur sont des middlewares qui s'appliquent uniquement aux erreurs. 

```js
app.use((err, req, res, next) => {
  console.log('Je suis un middleware de gestion d\'erreur')
  next()
})
```


## Exemples

### Exemple 1

- Voir `demo2-middleware` dans le dossier [demos-Express](https://livecegepfxgqc.sharepoint.com/:f:/s/AECDevWeb-2022-2024-2-Programmation1/EoRvpuqnahlEuMk1CPhf3lYBywYDIynnvMqZaumCoYDLsg?e=LGDDbH)

![HTTP](./img/middleware2.png){data-zoomable}

1 - La requête arrive sur le serveur. Le middleware global est exécuté.

Il affiche l'url (`req.url`) dans la console et appelle la fonction next() pour passer la requête au middleware suivant.

2- Le middleware suivant est exécuté. Il affiche un message dans la console et envoie une réponse au client.

::: tip Important
Ces deux middlewares sont définis avec la méthode `app.use()` donc ils seront exécutés quelque soit la route et la méthode HTTP utilisée. 
:::

### Exemple 2

- Voir `demo2-middleware-ex2` dans le dossier [demos-Express](https://livecegepfxgqc.sharepoint.com/:f:/s/AECDevWeb-2022-2024-2-Programmation1/EoRvpuqnahlEuMk1CPhf3lYBywYDIynnvMqZaumCoYDLsg?e=LGDDbH)


Tester le code suivant :

```js
const express = require('express');
const app = express();

app.use('/', (req, res, next) => {
    res.send('<h1>Page d\'accueil</h1>');
});

app.use('/users', (req, res, next) => {
    res.send('<h1>Page Users !</h1>');
});

app.listen(3000, () => {
console.log('Le serveur écoute sur le port 3000');
});
```

Tester les urls suivantes :

- `http://localhost:3000/`
- `http://localhost:3000/users`

Quel est le problème ?

Que faut-il faire pour corriger le problème ?

::: details Correction
En utilisant la méthode `app.use()`, les middlewares sont exécutés dans l'ordre dans lequel ils sont définis.

En effet la route `/` signifie que toutes les urls commençant par `/` sont concernées.

Il faut donc inverser l'ordre des middlewares.

```js
app.use('/users', (req, res, next) => {
    res.send('<h1>Page Users !</h1>');
});

app.use('/', (req, res, next) => {
    res.send('<h1>Page d\'accueil</h1>');
});
```

Cependant pour éviter ce problème, il est préférable d'utiliser la méthode `app.get()` pour définir les middlewares de routage.

```js
app.get('/', (req, res, next) => {
    res.send('<h1>Page d\'accueil</h1>');
});

app.get('/users', (req, res, next) => {
    res.send('<h1>Page Users !</h1>');
});
```

Ainsi l'ordre des middlewares n'a plus d'importance.
:::


## Middleware d'analyse de requête (parser)

- Voir `demo3-form-data` dans le dossier [demos-Express](https://livecegepfxgqc.sharepoint.com/:f:/s/AECDevWeb-2022-2024-2-Programmation1/EoRvpuqnahlEuMk1CPhf3lYBywYDIynnvMqZaumCoYDLsg?e=LGDDbH)


Les middlewares d'analyse de requête (parser) sont des middlewares qui analysent les données de la requête et les mettent dans l'objet `req.body`.

Ils sont utilisés par exemple pour récupérer les données d'un **formulaire HTML**.

Le middleware d'analyse de requête va analyser les données du formulaire et les mettre dans l'objet `req.body`.

Exemple d'utilisation du middleware `express.urlencoded` :

![HTTP](./img/middleware3.png){data-zoomable}

1- `express.urlencode` est un middleware inclus dans le framework Express. [Voir la documentation](https://expressjs.com/en/4x/api.html#express.urlencoded)

2- Ce middleware est exécuté quand une requête POST arrive sur le serveur, c'est à dire que le formulaire est envoyé.

`req.body` est un objet qui contient les données du formulaire.

3- Le `name="title"` défini la valeur title de l'objet `req.body`. On pourra donc accéder à la valeur du champ `title` avec `req.body.title`.

---

La page `/add-post` contient un formulaire HTML avec un champ `title`

![HTTP](./img/middleware4.png){data-zoomable}

---

Dans la console on peut voir que l'objet `req.body` contient les données du formulaire.
![HTTP](./img/middleware5.png){data-zoomable}

