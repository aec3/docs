# Service Web

## Introduction

Un service Web est un cadre permettant à des applications écrites dans différents langages de programmation de communiquer entre elles via Internet.
- La communication est manipulée par le protocole HTTP.
- Plusieurs standardisations: SOAP, REST, Odata...
- Nous utiliserons l’architecture REST pour ce cours.

::: tip Info
Les services Web sont souvent appelés API (interface de programmation d'application). Bien que les deux termes soient souvent utilisés de manière interchangeable, il existe une différence entre les deux. Nous verrons cela plus tard, pour le moment nous pourront utiliser les deux termes.
:::



![REST](./img/sw1.png)

## Exemples de services Web

- Cotes boursières.
- Taux de change.
- ChatGPT.
- Validations : adresse de courriel, adresse postale en fonction du code postal, etc.
- Correction de l'orthographe et suggestions.
- Librairie.
- Réseaux sociaux.
- Recherche sur Internet.
- Envoi de SMS.
- Traduction.
- Statistiques.
- Données météorologiques.

### Exemple application météo

![REST](./img/sw2.png){ data-zoomable width=25% }


L'application météo de votre téléphone contacte régulièrement un service Web pour mettre à jour les informations météo suivant votre position. 

Il existe beaucoup de services Web, gratuits ou payants, qui propose ce service. Par exemple, https://openweathermap.org est très connu et il est utilisé par des applications qui ont besoin de connaître la météo d'une ville.

Ce service renvoie les données au format JSON et elles sont ensuite traitées par l'appication pour être affichées sur le téléphone.

L'API est accessible via l'url https://api.openweathermap.org/data/2.5/weather?q=montreal&appid=YOUR_API_KEY

Il est possible de voir les données météo de n'importe quelle ville en changeant le paramètre `q` dans l'url. Par exemple, pour voir les données météo de Québec, on utilise l'url https://api.openweathermap.org/data/2.5/weather?q=quebec&appid=YOUR_API_KEY

`YOUR_API_KEY` est une clé d'authentification qui permet de limiter l'accès au service Web. Il faut s'inscrire sur le site pour obtenir une clé.


### Exemple Twitter

Plusieurs applications sont utilisées pour accéder à Twitter. Applications Android, IOS, 

Ces applications ont besoins d'accéder aux données de la base de données de Twitter.

Bien sûr, Twitter ne va pas donner accès directement à sa base de données à n'importe qui. 

Ces applications vont donc utiliser le service Web que Twitter met à dispoition (mais payant !) pour accéder aux données des utilisateurs.

![REST](./img/sw3.png){data-zoomable}

L'API est accesible via une url : https://api.twitter.com/2/
puis on ajoute la route que l'on veut utiliser.

les routes se nomment des **endpoints**.

Pour l'exemple, voir la liste des endpoints :
https://developer.twitter.com/en/docs/api-reference-index#twitter-api-v2

Par exemple, pour voir les tweets d'un utilisateur, on utilise l'endpoint `/users/:id/tweets`

`:id` est un paramètre qui permet de spécifier l'id de l'utilisateur.

Il est possible de voir les tweets, de poster des nouveaux tweets, de voir les utilisateurs, de voir les abonnements, etc.

---

Suivant les API, vous pourrez faire les mêmes actions que nous avons fait avec la base de données MongoDB. Le fameux CRUD :
- **C**reate : Créer
- **R**ead : Lire
- **U**pdate : Modifier
- **D**elete : Supprimer

---

::: tip Info
Nous n'allons pas utiliser l'API de Twitter, mais nous allons créer notre propre API plus tard dans le cours.
:::


## Architecture REST

Pour créer un service Web, il faut respecter une architecture.

L'architecture REST est une façon de concevoir les interactions entre notre application et un service Web.

REST signifie **RE**presentational **S**tate **T**ransfer.

REST est un style d'architecture qui définit un ensemble de contraintes et de propriétés basées sur HTTP.

Pour le moment nous n'allons pas voir la norme en détail, retenez que le cours est basé sur cette architecture.

Les API REST basées sur HTTP sont définies par :

- un URI de base, comme https://api.twitter.com/2/ ;
- des méthodes HTTP standards (par ex. : GET, POST, PUT, PATCH et DELETE) ;
  - GET : Consultation d’une ressource
  - POST : Création d’une ressource
  - PUT : Remplace une ressource
  - PATCH : Modification d’une ressource
  - DELETE : Destruction d’une ressource 
- Un type de média (type MIME). Nous utiliserons le type JSON.
- Les routes (ou endpoints) sont les URL utilisées pour faire les requêtes à une API.

![REST](./img/sw4.png){data-zoomable width=50% }



